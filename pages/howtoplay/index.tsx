import HowToPlay from 'components/home/HowToPlay';
import React from 'react';

const HowToPlayView = () => {
  return (
    <div className="wrapper">
      <HowToPlay />
    </div>
  );
};

export default HowToPlayView;
