import 'styles/globals.scss';
import 'styles/header.scss';
import 'styles/home-page.scss';
import 'styles/responsive.scss';

import type { AppProps } from 'next/app';
import Layout from 'components/Layout';
import { QueryClient, QueryClientProvider } from 'react-query';
import StateContextProvider from 'context/StateContext';
import styled from 'styled-components';
import InitialStateContext from 'components/common/InitialStateContext';
import { WalletSelectorContextProvider } from 'context/WalletContext';

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={queryClient}>
      <WalletSelectorContextProvider>
        <StateContextProvider>
          <BGApp>
            <InitialStateContext />
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </BGApp>
        </StateContextProvider>
      </WalletSelectorContextProvider>
    </QueryClientProvider>
  );
}

export default MyApp;

const BGApp = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
  background-color: #05231b;
  background-size: cover;
  overflow: hidden;
  color: whitesmoke;
`;
