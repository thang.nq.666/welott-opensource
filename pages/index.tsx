/* eslint-disable @typescript-eslint/no-explicit-any */
import type { NextPage } from 'next';
import Image from 'next/image';
import useActionValidate from 'hooks/useActionValidate';
import { EModal } from 'define/enum';
import ShowTicketModal from 'components/modals/ShowTicketModal';
import dynamic from 'next/dynamic';
import Head from 'next/head';

const CurrentLottery = dynamic(() => import('components/home/current-lottery'), { ssr: false });
const PreviousLottery = dynamic(() => import('components/home/PreviousLottery'), { ssr: false });
const AllHistory = dynamic(() => import('components/home/AllHistory'), { ssr: false });
import YourTicket from 'components/home/YourTicket';
import BuyTicketModal from 'components/modals/BuyTicketModal';
import RequireLoginModal from 'components/modals/RequireLoginModal';
import RequireRegisterModal from 'components/modals/RequireRegisterModal';

import build_on_near from 'public/images/build-on-near.png';
import buy_ticket_btn from 'public/images/buy-ticket-btn.png';
import deco_1 from 'public/images/deco-1.png';
import deco_2 from 'public/images/deco-2.png';
import deco_3 from 'public/images/deco-3.png';
import deco_4 from 'public/images/deco-4.png';
import deco_5 from 'public/images/deco-5.png';
import deco_6 from 'public/images/deco-6.png';
// import Lottery from 'public/images/LOTTERY.png';
import Welott from 'public/images/WELOTT.png';
import useInitPrice from 'hooks/useInitPrice';
import bg_home_top from 'public/images/bg-home-top.png';
import bg_home_top_mobile from 'public/images/bg-head-top-mobile.jpg';
import { useWindowDimensions } from 'hooks/useWindowDimension';
import HowToPlay from 'components/home/HowToPlay';
import LotteryDetailModal from 'components/modals/LotteryDetailModal';
import ReportFeedback from 'components/common/ReportFeedback';

const TopInfo = () => {
  return (
    <div className={'top_info'}>
      <h4 className={'build_on_near'}>
        <a href="https://near.org/" target={'_blank'} rel="noreferrer">
          <Image priority={true} src={build_on_near} width={153} height={20} alt="Build on Near" />
        </a>
      </h4>
      {/* <h4 className={'near_lott'}>Blockchain-based</h4> */}

      <div className="lottery">
        <Image priority={true} src={Welott} width={392} height={137} alt="Welott" layout="responsive" />
        <div className="slash-light"></div>
      </div>
      <p className="rule-thin">Win every 12 hours if 1.2.3.4.5.6 of your ticket numbers match!</p>
    </div>
  );
};

const Deco = () => {
  return (
    <>
      <p
        className="deco-1 ball-animated-transform"
        style={{
          animationDuration: '7s',
        }}
      >
        <Image priority={true} src={deco_1} width={54} height={54} alt="Deco 1" />
      </p>
      <p
        style={{
          animationDuration: '12s',
        }}
        className="deco-2 ball-animated-transform"
      >
        <Image priority={true} src={deco_2} width={70} height={68} alt="Deco 2" />
      </p>
      <p className="deco-3 stage">
        <span className="bouncer-holder">
          <Image priority={true} src={deco_3} width={147} height={147} alt="Near logo" />
        </span>
      </p>
      <p className="deco-4 ball-animated-transform">
        <Image priority={true} src={deco_4} width={57} height={53} alt="Deco 4" />
      </p>
      <p
        style={{
          animationDuration: '15s',
        }}
        className="deco-5 ball-animated-transform"
      >
        <Image priority={true} src={deco_5} width={61} height={61} alt="Deco 5" />
      </p>
      <p className="deco-6 ball-animated-transform-scale">
        <a href="https://unchain.fund/" target={'_blank'} rel="noreferrer">
          <Image priority={true} src={deco_6} width={197} height={192} alt="Unchain fund" />
        </a>
      </p>
    </>
  );
};

const LangBiangBg = () => {
  return <div className="lang-biang"></div>;
};

const Home: NextPage = () => {
  const { handleOpenModal } = useActionValidate();

  const getSize = useWindowDimensions();
  const isAppendMobileMenu = getSize?.width <= 600 || false;

  useInitPrice();
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>Welott - NEAR based Lottery</title>
      </Head>
      <div id="home" className="home">
        <div
          className="bg-top"
          style={{
            backgroundImage: `url(${!isAppendMobileMenu ? bg_home_top.src : bg_home_top_mobile.src})`,
            backgroundSize: 'cover',
          }}
        >
          {/* <div className="bg-top" style={{ backgroundImage: `url(${bg_wave.src})`, backgroundSize: 'cover' }}> */}
          <div className="wrapper">
            <TopInfo />
            <CurrentLottery />
            <button onClick={() => handleOpenModal(EModal.BUY_TICKET)} className="home_buy_ticket">
              <Image priority={true} src={buy_ticket_btn} alt="Buy ticket" width={275} height={107} />
            </button>
            <LangBiangBg />
            <Deco />
            <PreviousLottery />
          </div>
          {/* </div> */}
        </div>
        <div className="wrapper">
          <YourTicket />
          <AllHistory />
          <HowToPlay />
          <BuyTicketModal />
          <LotteryDetailModal />
          <RequireLoginModal />
          <RequireRegisterModal />
          <ShowTicketModal />
        </div>
        <div className="btn-feedback">
          <ReportFeedback />
        </div>
      </div>
    </>
  );
};

export default Home;
