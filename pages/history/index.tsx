import AllHistory from 'components/home/AllHistory';
import ClaimTokenModal from 'components/modals/ClaimTicketModal';
import React from 'react';
import RequireLoginModal from 'components/modals/RequireLoginModal';
import RequireRegisterModal from 'components/modals/RequireRegisterModal';
import LotteryDetailModal from 'components/modals/LotteryDetailModal';

const HistoryView = () => {
  return (
    <>
      <div className="wrapper">
        <AllHistory />
      </div>
      <ClaimTokenModal />
      <LotteryDetailModal />
      <RequireLoginModal />
      <RequireRegisterModal />
    </>
  );
};

export default HistoryView;
