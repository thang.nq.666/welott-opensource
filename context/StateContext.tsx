/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { createContext, useReducer, ReactElement, useContext, useEffect } from 'react';
import Image from 'next/image';
import { Spin } from 'antd';
import logo_nel from 'public/images/logo_nel_transparent.png';
import { EAction, EModal } from 'define/enum';
import { IAction, IContext, IRoundUserInfo, IState, IUserNearBalance, IUserStatistic } from 'define/interface';
import * as nearAPI from 'near-api-js';

const {
  LOGIN,
  LOGOUT,
  INIT_CONTRACT,
  INIT_CONTRACT_ORACLE,
  INIT_WALLET,
  TOGGLE_MODAL,
  ON_LOADING,
  OFF_LOADING,
  INIT_NEAR,
  INIT_USER_BALANCE,
  INIT_USER_STATISTIC,
  INIT_PAGING_HISTORY,
  SET_CURRENT_LOTTERY_ID,
  SET_LOTTERY_INFO_USER,
} = EAction;

const initState: IContext = {
  nameOpenModal: EModal.NONE,
  isLoading: true,
  isLogin: false,
  dispatch: () => {},
  isModalOpen: false,
  currentLotteryId: 1,
  userNearBalance: {
    available: '0',
    staked: '0',
    stateStaked: '0',
    total: '0',
  },
  userStatistic: {
    lotteries_won: [],
    lotteries_joined: [],
    near_amount_can_claim: undefined,
    n_tickets_won: undefined,
    n_total_tickets: undefined,
  },
  roundUserInfo: [],
  pagingHistory: {
    total: 0,
    total_pages: 0,
    item_per_page: 0,
  },
};

function appReducer(state: IState, action: IAction): IState {
  const { type, payload } = action;

  switch (type) {
    case INIT_CONTRACT: {
      return { ...state, contract: payload as unknown as nearAPI.Contract };
    }
    case INIT_CONTRACT_ORACLE: {
      return { ...state, contract_oracle: payload as unknown as nearAPI.Contract };
    }
    case INIT_WALLET: {
      return { ...state, wallet: payload as unknown as nearAPI.WalletConnection };
    }
    case INIT_NEAR: {
      return { ...state, near: payload as unknown as nearAPI.Near };
    }
    case INIT_USER_BALANCE: {
      return { ...state, userNearBalance: payload as unknown as IUserNearBalance };
    }
    case INIT_USER_STATISTIC: {
      return { ...state, userStatistic: payload as unknown as IUserStatistic };
    }
    case INIT_PAGING_HISTORY: {
      return { ...state, pagingHistory: payload as unknown as any };
    }
    case SET_LOTTERY_INFO_USER: {
      const mod_final_result: IRoundUserInfo[] = [
        // ...state.roundUserInfo,
        ...(payload as unknown as IRoundUserInfo[]),
      ].filter((item, idx, a) => a.findIndex((t) => t.id === item.id) === idx);
      return { ...state, roundUserInfo: mod_final_result };
    }
    case LOGIN: {
      return { ...state, isLogin: true };
    }
    case LOGOUT: {
      return { ...state, isLogin: false, userNearBalance: initState.userNearBalance };
    }
    case ON_LOADING: {
      return { ...state, isLoading: true };
    }
    case OFF_LOADING: {
      return { ...state, isLoading: false };
    }
    case SET_CURRENT_LOTTERY_ID: {
      return { ...state, currentLotteryId: payload as unknown as number };
    }
    case TOGGLE_MODAL: {
      return {
        ...state,
        nameOpenModal: payload as unknown as EModal,
      };
    }
    default:
      return state;
  }
}

export const StateContext = createContext(initState);

const StateContextProvider = ({ children }: { children: ReactElement }) => {
  const [state, dispatch] = useReducer(appReducer, initState);

  useEffect(() => {
    Promise.all(
      Array.from(document.images)
        .filter((img) => !img.complete)
        .map(
          (img) =>
            new Promise((resolve) => {
              img.onload = img.onerror = resolve;
            })
        )
    ).then(() => {
      console.log('all images finished loading');
      dispatch({ type: EAction.OFF_LOADING });
    });
  }, []);

  return (
    <StateContext.Provider
      value={{
        ...state,
        dispatch,
      }}
    >
      <Spin
        tip={<p className="loading-text">Loading...</p>}
        size="small"
        spinning={state.isLoading}
        indicator={<Indicator />}
        wrapperClassName="loading-spin-wrapper"
      >
        {children}
      </Spin>
    </StateContext.Provider>
  );
};

export const useRealContext = () => useContext(StateContext);

export default StateContextProvider;

const Indicator = () => {
  return (
    <div className="indicator">
      <div
        style={{
          // backgroundColor: 'white',
          // padding: '7px',
          width: 40,
          height: 40,
          borderRadius: '100%',
          margin: '0 auto',
          overflow: 'hidden',
          pointerEvents: 'none',
        }}
      >
        <Image priority={true} src={logo_nel} className="loading-spin-icon" alt="loading" width={74} height={74} />
      </div>
    </div>
  );
};
