/* eslint-disable @next/next/no-page-custom-font */
import Head from 'next/head';
import React, { ReactElement } from 'react';
import Footer from './Footer';
import Header from './Header';

const Layout = ({ children }: { children: ReactElement }) => {
  return (
    <>
      <Head>
        <title>Welott</title>
        <meta name="description" content={'Welott'} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      {children}
      <Footer />
    </>
  );
};

export default Layout;
