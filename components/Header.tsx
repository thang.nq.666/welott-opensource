import { Image } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { StateContext } from 'context/StateContext';
import { useCallback, useContext, useEffect, useState } from 'react';
import useFunctionContract from 'hooks/useFunctionContract';
import { useWindowDimensions } from 'hooks/useWindowDimension';
import Footer from './Footer';
import { EBreakPoint } from 'define/enum';
import { formatAccountName } from 'utils/common';
import { useWalletSelector } from 'context/WalletContext';

const Header = () => {
  const [open, setOpen] = useState(false);

  const { handleNearLogin, handleNearLogOut } = useFunctionContract();
  const { accountId } = useWalletSelector() || {};

  const [accountName, setAccountName] = useState('');

  const { isLogin } = useContext(StateContext);
  const { width } = useWindowDimensions();
  const router = useRouter();
  const [activemenuCurrent, setActivemenuCurrent] = useState(router.asPath);

  const isAppendMobileMenu = width <= EBreakPoint.MOBILE || false;

  const handleActiveId = (id: string) => {
    setActivemenuCurrent(`/${id}`);
  };

  const drawMobileMenu = useCallback(() => {
    if (typeof window !== 'undefined') {
      if (open) {
        setOpen(false);
        document.body.style.overflow = 'auto';
      } else {
        setOpen(true);
        document.body.style.overflow = 'hidden';
      }
    }
  }, [open]);

  const _initAccountName = useCallback(() => {
    if (!accountId) return;
    setAccountName(accountId);
  }, [accountId]);

  useEffect(() => {
    _initAccountName();
  }, [_initAccountName, isLogin]);

  const ConnectWalletComp = () => (
    <div className={`menu-btn button-style menus`}>
      <span className="btn-connect" onClick={!isLogin ? () => handleNearLogin() : () => null}>
        <span className="btn-label">{`${isLogin ? formatAccountName(accountName as string) : 'Connect Wallet'}`}</span>
      </span>
      {isLogin ? (
        <div className="connect-extend">
          <div className={`menu-btn button-style menus`}>
            <div className="btn-connect">
              {/* <div onClick={!isLogin ? () => handleNearLogin() : () => null} className="sign-out">
                Switch Wallet
              </div>
              <div className="line-btn"></div> */}
              <div onClick={handleNearLogOut} className="sign-out">
                Sign Out
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );

  return (
    <div id="header" className={'wrap_header'}>
      {isAppendMobileMenu ? (
        <>
          <div style={{ cursor: 'pointer', width: '100%' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <div onClick={drawMobileMenu}>
                <Image
                  src={'/images/mobileMenu.svg'}
                  alt="mobileMenu"
                  width={30}
                  height={30}
                  preview={{ visible: false, mask: false }}
                  // style={{ opacity: open ? '0' : '1' }}
                />
              </div>
              <Link href={'/'}>
                <Image
                  src={`/images/welott_logo.svg`}
                  alt="mobileMenu"
                  width={100}
                  preview={{ visible: false, mask: false }}
                  // style={{ opacity: open ? '0' : '1' }}
                />
              </Link>
            </div>
          </div>
          {open ? (
            <DrawOverwrite id="mobile-menu-only">
              <div className="mask-mobile" />
              <div className="menu-mobile-responsive-content">
                <div className="top-header-menu-mobile-responsive">
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      gap: 30,
                    }}
                  >
                    <div onClick={drawMobileMenu}>
                      <Image
                        src={'/images/closeable.svg'}
                        alt="mobileMenu"
                        height={20}
                        preview={{ visible: false, mask: false }}
                      />
                    </div>
                    <Link href={'/'}>
                      <Image
                        src={`/images/welott_logo.svg`}
                        alt="mobileMenu"
                        width={100}
                        preview={{ visible: false, mask: false }}
                      />
                    </Link>
                  </div>
                </div>
                <MobileMenuWrapper>
                  {menus.map(({ pathName, pathRoute, idElement }, idx) => {
                    const activeClass = router.asPath === pathRoute ? 'active' : '';
                    return (
                      <li onClick={drawMobileMenu} className={` item-menu ${activeClass}`} key={idx}>
                        <a href={idElement}>{pathName}</a>
                      </li>
                    );
                  })}
                  {accountName ? (
                    <>
                      <li className="item-menu" onClick={() => handleNearLogOut()}>
                        <span className="item-menu-route"> Sign out</span>
                      </li>
                      <LineBreak />
                      <div className="mobile-btn-responsive">
                        <span className="wallet-name">{accountName}</span>
                      </div>
                    </>
                  ) : (
                    <>
                      <LineBreak />
                      <li className="item-menu" onClick={() => handleNearLogin()}>
                        <span className="item-menu-route"> Sign In</span>
                      </li>
                    </>
                  )}
                </MobileMenuWrapper>
                <AbsoluteMobieFooter>
                  <Footer />
                </AbsoluteMobieFooter>
              </div>
            </DrawOverwrite>
          ) : null}
        </>
      ) : (
        <>
          <div className={`header-wrapper middle`}>
            <Link href={'/'}>
              <div className="header-logo">
                <Image
                  src={`/images/welott_logo.svg`}
                  alt="mobileMenu"
                  width={100}
                  preview={{ visible: false, mask: false }}
                />
              </div>
            </Link>
            <nav className="middle-menu-bar">
              <ul>
                {menus.map(({ pathName, idElement }, idx) => {
                  const activeClass = activemenuCurrent === `/${idElement}` ? 'active' : '';
                  return (
                    <li
                      key={idx}
                      className={`${idx === 0 && activemenuCurrent === '/' ? 'active' : ''} ${activeClass}`}
                      onClick={() => handleActiveId(idElement)}
                    >
                      <a href={idElement}>{pathName}</a>
                    </li>
                  );
                })}
              </ul>
            </nav>
            <ConnectWalletComp />
          </div>
        </>
      )}
    </div>
  );
};

export default Header;

const menus = [
  {
    pathName: 'Home',
    pathRoute: '/',
    idElement: '#',
  },
  {
    pathName: 'History',
    pathRoute: '/history',
    idElement: '#all-history',
  },
  {
    pathName: 'How to play',
    pathRoute: '/howtoplay',
    idElement: '#how-to-play',
  },
  // {
  //   pathName: 'Charity',
  //   pathRoute: '/charity',
  // },
];

const LineBreak = styled.div`
  width: 90%;
  height: 1px;
  background-color: rgba(249, 250, 250, 0.1);
  margin: 10px 0;
  position: relative;
`;

const AbsoluteMobieFooter = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
`;

const MobileMenuWrapper = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  padding-left: 24px;
  padding-top: 24px;
  font-size: 20px;
  .item-menu {
    cursor: pointer;
  }
  .item-menu:not(:last-child) {
    margin-bottom: 15px;
  }
  .item-menu:nth-child(5) {
    margin-bottom: 0;
  }
  a,
  .item-menu-route {
    font-weight: 400;
  }
  .active {
    color: var(--greenlight);
    font-weight: 600;
  }
  .mobile-btn-responsive {
    margin-top: 24px;
    .wallet-name {
      border: 1px solid var(--greenlight);
      border-radius: 10px;
      text-align: center;
      display: inline-block;
      padding: 5px 10px;
      background: linear-gradient(180deg, rgba(84, 169, 67, 0) 0%, rgba(84, 169, 67, 0.2) 100%), rgba(84, 169, 67, 0.1);
      color: var(--white);
      font-weight: 400;
      font-size: 18px;
      max-width: 250px;
      width: 100%;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`;

const DrawOverwrite = styled.div`
  position: fixed;
  inset: 0;
  z-index: 1000;
  pointer-events: none;
  transition: all 0.3s ease;
  .mask-mobile {
    position: absolute;
    inset: 0;
    z-index: 1000;
    background: rgba(0, 0, 0, 0.45);
    pointer-events: auto;
  }

  .menu-mobile-responsive-content {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    box-shadow: -6px 0 16px -8px #00000014, -9px 0 28px #0000000d, -12px 0 48px 16px #00000008;
    background-color: #001612;
    transition: all 0.3s ease;
    z-index: 1001;
    pointer-events: all;
    width: 100%;
  }
  .top-header-menu-mobile-responsive {
    padding: 15px;
    border-bottom: 1px solid rgba(249, 250, 250, 0.1);
  }
`;

export const ConnectButton = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 18px;
  gap: 10px;
  white-space: nowrap;
  background: linear-gradient(180deg, rgba(84, 169, 67, 0) 0%, rgba(84, 169, 67, 0.2) 100%), rgba(84, 169, 67, 0.1);
  border-radius: 6px;
  border: 1px solid var(--greenlight);
  margin-left: 30px;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    margin-left: 0;
  }
`;
