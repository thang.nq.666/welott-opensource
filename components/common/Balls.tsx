import React from 'react';
import { colorsMapping } from 'utils/common';

interface IBallProps {
  ballNumber: number;
  width?: number;
  height?: number;
  customStyle?: unknown;
}

const Balls = (props: IBallProps) => {
  const { ballNumber, width, height, customStyle } = props;
  const bgColor = colorsMapping(ballNumber);

  return (
    <div
      className="ball-class-api"
      style={{
        background: bgColor,
        color: 'white',
        width: width ? width : 50,
        height: height ? height : 50,
        borderRadius: '100%',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 600,
        ...(customStyle as IBallProps),
      }}
    >
      {ballNumber}
    </div>
  );
};

export default Balls;
