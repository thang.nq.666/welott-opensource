import useInitPrice from 'hooks/useInitPrice';
import useInitContract from 'hooks/useInitContract';
import useInitWithApi from 'hooks/useInitWithApi';

const InitialStateContext = () => {
  useInitPrice();
  useInitContract();
  useInitWithApi();
  return null;
};

export default InitialStateContext;
