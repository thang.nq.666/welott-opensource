import Image from 'next/image';
import arrow_right_top from 'public/images/arrow-right-top.png';

interface IProps {
  title?: string;
  url?: string;
}
const HashLink = ({ title = 'Near Hash', url = 'https://nearlenddao.com/' }: IProps) => {
  return (
    <a target="_blank" href={url} rel="noopener noreferrer">
      <div className="near-hash">
        <span className="text">{title}</span>
        <span>
          <Image priority={true} alt="Near hash" src={arrow_right_top} width={12} height={12} />
        </span>
      </div>
    </a>
  );
};

export default HashLink;
