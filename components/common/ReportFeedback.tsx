import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { BackTop } from 'antd';

const ReportFeedback = () => {
  const handleCloseMenuMobileOnliy = () => {
    // const getId = document.getElementById('mobile-menu-only');
    // if (getId) {
    //   getId?.remove();
    // }
  };
  return (
    <div className="feed-back-combine">
      {/* <Link target={'_blank'} href="https://forms.gle/pHjz6y7ciMwdgpUF8" rel="noreferrer">
        <a target="_blank">
          <Image alt="Feedback to Nearlend DAO" src="/images/feedback-btn.png" width={133} height={40} />
        </a>
      </Link> */}
      <Link target="_blank" href="https://forms.gle/V85T8D2VoHDcaGez5" rel="noreferrer">
        <a target="_blank">
          <Image alt="Report bug to Nearlend DAO" src="/images/reportbug-btn.png" width={133} height={40} />
        </a>
      </Link>
      <BackTop onClick={handleCloseMenuMobileOnliy}>
        <div
          style={{
            width: 35,
            height: 35,
            background:
              'linear-gradient(180deg, rgba(84, 169, 67, 0) 0%, rgba(84, 169, 67, 0.2) 100%), rgba(84, 169, 67, 0.1)',
            backdropFilter: 'blur(10px)',
            borderRadius: '50%',
            padding: '10px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            alt="back to top"
            style={{ position: 'relative', top: '-2px' }}
            src="/images/backtop.svg"
            width="100"
            height="100"
          />
        </div>
      </BackTop>
    </div>
  );
};

export default ReportFeedback;
