import type { NextPage } from 'next';
import { EAction, ELotteryStatus, EModal } from 'define/enum';
import Image from 'next/image';

import coin_near from 'public/images/coin-near.png';
import useActionValidate from 'hooks/useActionValidate';
import useFunctionContract from 'hooks/useFunctionContract';
import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { ICurrentLottery } from 'define/interface';
import { StateContext } from 'context/StateContext';
import { ENV_ID_TOKEN_wNEAR, tokenFormat } from 'utils/constant';
import { formatCashToView } from 'utils/common';
import ClaimTicketModal from 'components/modals/ClaimTicketModal';
import { Tooltip } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';
import { welottText } from 'utils/text';

const YourTicket: NextPage = () => {
  const { userNearBalance, roundUserInfo, dispatch, userStatistic } = useContext(StateContext);
  const { handleOpenModal } = useActionValidate();
  const { handleGetLastLotteryId, handleGetCurrentLotteryRun, handleGetFullUserTicketNumber } = useFunctionContract();

  const [currentLottery, setCurrentLottery] = useState<ICurrentLottery>();
  const [lastId, setLastId] = useState(0);
  const [ticketNumbers, setTicketNumbers] = useState<number[]>([]);
  const [totalWinningReward, setTotalWinningReward] = useState<number>();

  const current_price_per_ticket = useMemo(() => {
    const NEAR_DECIMALS =
      tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;
    return (currentLottery?.price_ticket_in_near as unknown as number) / Math.pow(10, NEAR_DECIMALS);
  }, [currentLottery?.price_ticket_in_near]);

  const _handleOpenBuyTicket = () => {
    handleOpenModal(EModal.BUY_TICKET);
  };

  const _handleOpenClaim = useCallback(() => {
    dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: lastId });
    handleOpenModal(EModal.CLAIM);
  }, [dispatch, handleOpenModal, lastId]);

  const _handleShowTicket = useCallback(async () => {
    handleOpenModal(EModal.SHOW_TICKET);
  }, [handleOpenModal]);

  const _initCurrentLottery = useCallback(async () => {
    const current = await handleGetCurrentLotteryRun(lastId);
    setCurrentLottery(current);
  }, [handleGetCurrentLotteryRun, lastId]);

  const _getUserTicketNumber = useCallback(async () => {
    const result = await handleGetFullUserTicketNumber(lastId);
    if (!result) return;
    setTicketNumbers(result);
  }, [handleGetFullUserTicketNumber, lastId]);

  const _getLastId = useCallback(async () => {
    const last_id = await handleGetLastLotteryId();
    setLastId(last_id);
  }, [handleGetLastLotteryId]);

  const _initUnclaimedRound = useCallback(async () => {
    if (!roundUserInfo) return setTotalWinningReward(0);
    const filter_claimable = roundUserInfo?.filter((item) =>
      item.near_in_rewards.find((lott) => Number(lott.status) === ELotteryStatus.CLAIMABLE)
    );

    const total_winning = filter_claimable.reduce((prev, curr) => {
      prev += curr.near_amount_can_claim;
      return prev;
    }, 0);

    setTotalWinningReward(total_winning);
  }, [roundUserInfo]);

  const _disableBuyTicket = useMemo(() => {
    const user_balance = Number(formatCashToView(Number(userNearBalance.available), 8));
    const amount_ticket_bought = ticketNumbers?.length || 0;

    if (amount_ticket_bought === 120 || user_balance < 1) return true;
    return false;
  }, [ticketNumbers?.length, userNearBalance.available]);

  const _toDollar = useCallback((numb: string) => {
    return Number(Number(numb).toFixed(2)) * (tokenFormat[ENV_ID_TOKEN_wNEAR].usd || 1) || 0.0;
  }, []);

  useEffect(() => {
    _getLastId();
  }, [_getLastId]);

  useEffect(() => {
    _getUserTicketNumber();
  }, [_getUserTicketNumber]);

  useEffect(() => {
    _initCurrentLottery();
  }, [_initCurrentLottery]);

  useEffect(() => {
    _initUnclaimedRound();
  }, [_initUnclaimedRound]);

  return (
    <div className="your-ticket-section">
      <h3 className="big-title">Your Tickets</h3>
      <div className="wrap-info-ticket">
        <div className="ticket">
          <p className="desc">
            You have <span className="cl-yellow fwb">{userStatistic?.n_tickets_won || 0}</span> winning tickets
          </p>
          <div className="flex-align-center">
            <p className="mini-title">Total Prize</p>
            <div className="logo-amount">
              <Image priority={true} src={coin_near} width={30} height={30} alt="Near coin" />
              <p className="price-near">
                {`${totalWinningReward ? formatCashToView(totalWinningReward, 10) : 0} $NEAR`}
              </p>
            </div>
            <p className="price-dollar">
              ${totalWinningReward ? formatCashToView(_toDollar(totalWinningReward?.toString()), 8) : 0}
            </p>
            <button onClick={_handleOpenClaim} className="common-button yellow">
              View Tickets
            </button>
          </div>
        </div>
        <div className="ticket">
          <p className="desc">
            You have <span className="cl-yellow fwb">{ticketNumbers?.length || 0}</span> activated tickets{' '}
            <Tooltip arrowPointAtCenter color="#0000006b" title={welottText.max_120} placement="bottom">
              <InfoCircleFilled />
            </Tooltip>
          </p>
          <div className="flex-row">
            <div className="flex-align-center">
              <p className="mini-title">Wallet balance</p>
              <div className="logo-amount">
                <Image priority={true} src={coin_near} width={30} height={30} alt="Near coin" />
                <p className="price-near">{formatCashToView(Number(userNearBalance.available), 8)} $NEAR</p>
              </div>
              <div className="logo-amount-wrap-price">
                <p className="price-dollar">${formatCashToView(Number(_toDollar(userNearBalance.available)), 8)}</p>
              </div>
              <button
                onClick={_handleShowTicket}
                className={`common-button ${!ticketNumbers?.length ? 'btn-disable' : ''}`}
              >
                Show tickets
              </button>
            </div>
            <div className="flex-align-center">
              <p className="mini-title">
                price per ticket{' '}
                <Tooltip arrowPointAtCenter color="#0000006b" title={welottText.price_per_ticket} placement="bottom">
                  <InfoCircleFilled />
                </Tooltip>
              </p>
              <div className="logo-amount">
                <Image priority={true} src={coin_near} width={30} height={30} alt="Near coin" />
                <p className="price-near">{Number(formatCashToView(current_price_per_ticket, 8))} $NEAR</p>
              </div>
              <div className="logo-amount-wrap-price">
                <p className="price-dollar">${formatCashToView(_toDollar(current_price_per_ticket.toString()), 8)}</p>
              </div>
              <button
                className={`common-button yellow ${_disableBuyTicket ? 'btn-disable' : ''}`}
                onClick={_handleOpenBuyTicket}
              >
                Buy tickets
              </button>
            </div>
          </div>
        </div>
      </div>
      <ClaimTicketModal />
    </div>
  );
};

export default YourTicket;
