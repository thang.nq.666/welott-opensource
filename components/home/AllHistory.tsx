import { StateContext } from 'context/StateContext';
import { IRoundUserInfo } from 'define/interface';
import useFunctionContract from 'hooks/useFunctionContract';
import { useCallback, useContext, useEffect, useState } from 'react';
import HistoryItem from './HistoryItem';
// import { CaretDownOutlined, CaretUpOutlined } from '@ant-design/icons';
import type { PaginationProps } from 'antd';
import { Pagination } from 'antd';
import useInitWithApi from 'hooks/useInitWithApi';
import styled from 'styled-components';

const AllHistory = () => {
  // const { width } = useWindowDimensions();
  // const isLoadMore = useMemo(() => width <= 600, [width]);

  const { roundUserInfo, isLogin, pagingHistory } = useContext(StateContext);
  const { handleNearLogin } = useFunctionContract();
  const { _initRoundUserInfo } = useInitWithApi();

  // const [showMore, setShowMore] = useState(false);
  const [current, setCurrent] = useState(1);

  const _initHistories = useCallback(() => {
    if (!roundUserInfo) return;
  }, [roundUserInfo]);

  // const _isLoadMore = useMemo(() => {
  //   return roundUserInfo.length < (pagingHistory.total || 0);
  // }, [pagingHistory.total, roundUserInfo.length]);

  // const _handleLoadMore = useCallback(() => {
  //   _initRoundUserInfo({ skip: histories.length });
  //   const ele = document.getElementById('style-scrollBar');
  //   setTimeout(() => {
  //     ele?.scrollTo({ top: ele.scrollTop + 440, behavior: 'smooth' });
  //   }, 1300);
  // }, [_initRoundUserInfo, histories]);

  useEffect(() => {
    _initHistories();
  }, [_initHistories]);

  const onChange: PaginationProps['onChange'] = (page, pageSize) => {
    setCurrent(page);
    _initRoundUserInfo({ skip: pageSize * (page - 1), limit: pageSize });
  };
  return (
    <div id="all-history" className="all-history">
      <h3 className="big-title">All history</h3>
      <div className="wrap-scroll">
        <div className="wrap-all-history">
          <div className="wrap-title">
            <div className="block-title round">
              <h6>Round</h6>
            </div>
            <div className="block-title win-numbers">
              <h6>Winning numbers</h6>
            </div>
            <div className="block-title pot-sizes">
              <h6>Pot size</h6>
              <p>$NEAR</p>
            </div>
            <div className="block-title prize prize-1">
              <h6>#1</h6>
            </div>
            <div className="block-title prize prize-2">
              <h6>#2</h6>
            </div>
            <div className="block-title prize prize-3">
              <h6>#3</h6>
            </div>
            <div className="block-title prize prize-4">
              <h6>#4</h6>
            </div>
            <div className="block-title prize prize-5">
              <h6>#5</h6>
            </div>
            <div className="block-title prize prize-6">
              <h6>#6</h6>
            </div>
            <div className="block-title your-tickets">
              <h6>Your tickets</h6>
            </div>
          </div>

          <div id="style-scrollBar" className="wrap-history-item">
            {/* <div id="style-scrollBar" className="wrap-history-item" style={{ maxHeight: showMore ? '100%' : '700px' }}> */}
            {roundUserInfo.length ? (
              roundUserInfo?.map((item: IRoundUserInfo, idx: number) => <HistoryItem key={idx} item={item} />)
            ) : !isLogin ? (
              <p className="no-history">
                <span onClick={handleNearLogin}>Connect Wallet</span> to check all histories !
              </p>
            ) : (
              <p className="no-history">Not Available</p>
            )}

            {/* {_isLoadMore ? (
              <h6 className="btn-loadmore" onClick={_handleLoadMore}>
                Load more <CaretDownOutlined className="btn-loadmore-caret" />
              </h6>
            ) : null} */}
          </div>
          {/* {!isLogin ? null : (
            <>
              {isLoadMore ? (
                <div className="load-more cl-f-light-green" onClick={() => setShowMore((prev) => !prev)}>
                  {showMore ? (
                    <>
                      <CaretUpOutlined />
                      <span className="label">See less</span>
                    </>
                  ) : (
                    <>
                      <span className="label">See more</span> <CaretDownOutlined />
                    </>
                  )}
                </div>
              ) : null}
            </>
          )} */}
        </div>
      </div>
      {isLogin && pagingHistory?.item_per_page && pagingHistory?.total > 10 ? (
        <PaginWrapper>
          <Pagination
            defaultPageSize={pagingHistory?.item_per_page}
            current={current}
            onChange={onChange}
            total={pagingHistory?.total}
          />
        </PaginWrapper>
      ) : null}
    </div>
  );
};

export default AllHistory;

const PaginWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 25px;
  .ant-pagination {
    .ant-pagination-item {
      border-radius: 6px;
      background: rgba(73, 228, 84, 0.1);
      border: none;
      line-height: 32px;
      a {
        color: whitesmoke;
        font-weight: 700;
      }
    }
    .ant-pagination-item-active {
      background: var(--greenlightSecondary);
      a {
        color: whitesmoke !important;
      }
    }
    .ant-pagination-item-link {
      background: rgba(73, 228, 84, 0.1);
      border: none;
      color: whitesmoke;
      border-radius: 6px;
    }
    .ant-pagination-options {
      .ant-select-selector {
        background: rgba(73, 228, 84, 0.1);
        border: none;
        color: whitesmoke;
        border-radius: 6px;
      }
    }

    .anticon.anticon-double-right.ant-pagination-item-link-icon,
    .anticon.anticon-double-left.ant-pagination-item-link-icon {
      color: whitesmoke;
    }
    .ant-pagination-item-ellipsis {
      color: whitesmoke;
    }
  }
`;
