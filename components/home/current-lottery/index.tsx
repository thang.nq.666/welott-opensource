/* eslint-disable @typescript-eslint/no-explicit-any */
import type { NextPage } from 'next';
import useFunctionContract from 'hooks/useFunctionContract';
import { useCallback, useEffect, useState } from 'react';
import { ICurrentLottery } from 'define/interface';

import NextDraw from './NextDraw';
import CurrentPrize from './CurrentPrize';

const CurrentLottery: NextPage = () => {
  const { handleGetCurrentLotteryRun, handleGetLastLotteryId, handleGetNumberTicketsPerLottery } =
    useFunctionContract();

  const [currentLottery, setCurrentLottery] = useState<ICurrentLottery>();

  const getCurrentLottery = useCallback(async () => {
    const latest_id = await handleGetLastLotteryId();
    const get_current = await handleGetCurrentLotteryRun(latest_id);
    const amount_tickets = await handleGetNumberTicketsPerLottery(latest_id);
    if (!latest_id || !get_current) return;

    setCurrentLottery({
      ...get_current,
      lottery_id: latest_id,
      amount_tickets,
    });
  }, [handleGetCurrentLotteryRun, handleGetLastLotteryId, handleGetNumberTicketsPerLottery]);

  useEffect(() => {
    getCurrentLottery();
  }, [getCurrentLottery]);

  return (
    <div className={'wrap_current_lottery'}>
      <CurrentPrize currentLottery={currentLottery as unknown as ICurrentLottery} />
      <NextDraw currentLottery={currentLottery as unknown as ICurrentLottery} />
    </div>
  );
};

export default CurrentLottery;
