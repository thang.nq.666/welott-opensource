/* eslint-disable @typescript-eslint/no-explicit-any */
import Image from 'next/image';
import { useCallback, useMemo } from 'react';
import { formatCashToView } from 'utils/common';
import { ENV_ID_TOKEN_wNEAR, tokenFormat } from 'utils/constant';
import { ICurrentLottery } from 'define/interface';
import useTokenPrices from 'hooks/useTokenPrices';

// import coin_near from 'public/images/near3D.svg';

interface IProps {
  currentLottery: ICurrentLottery;
}

const CurrentPrize = ({ currentLottery }: IProps) => {
  const { tokenPrice } = useTokenPrices({ isVisible: true });

  const current_prices = useMemo(() => {
    const NEAR_DECIMALS =
      tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;
    return (currentLottery?.amount_collected_in_near as unknown as number) / Math.pow(10, NEAR_DECIMALS);
  }, [currentLottery?.amount_collected_in_near]);

  const current_prices_dollar = useCallback(
    (_price: number) => {
      return _price * (tokenPrice ? tokenPrice?.[ENV_ID_TOKEN_wNEAR] : 1);
    },
    [tokenPrice]
  );

  return (
    <div className="wrap-current">
      <div className={'current_info'}>
        <h6>Current Prizes</h6>
        <div className="current_info_value">
          <Image
            className="cirle-rotate-custom"
            priority={true}
            src="/images/near3D.svg"
            width={56}
            height={56}
            alt="Coin Near"
          />
          <p style={{ marginLeft: 20 }}>{formatCashToView(current_prices, 8)}</p>
        </div>
      </div>
      <h6 className="spec cl-yellow">
        {tokenPrice ? `~$${formatCashToView(current_prices_dollar(current_prices), 8)}` : 'Calculating...'}
      </h6>
    </div>
  );
};

export default CurrentPrize;
