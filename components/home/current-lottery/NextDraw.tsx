/* eslint-disable @typescript-eslint/no-explicit-any */
import Image from 'next/image';
import useFunctionContract from 'hooks/useFunctionContract';
import { useCallback, useEffect, useRef, useState } from 'react';
import { ICurrentLottery } from 'define/interface';

import clock from 'public/images/clock.png';

interface IProps {
  currentLottery: ICurrentLottery;
}

const initTimeRemain = {
  hh: '00',
  mm: '00',
  ss: '00',
};

const NextDraw = ({ currentLottery }: IProps) => {
  const countDownRef = useRef<any>();

  const { handleGetCurrentTimeOnBlockchain } = useFunctionContract();

  const [timeRemain, setTimeRemain] = useState(initTimeRemain);

  const [currentTimeBlockchain, setCurrentTimeBlockchain] = useState(0);

  const _timeRemaining = useCallback(async () => {
    const end_time = new Date((currentLottery?.end_time as unknown as number) / Math.pow(10, 6)).getTime();

    if (currentTimeBlockchain) {
      const time_remaining = (end_time - currentTimeBlockchain) / 1000;
      if (time_remaining < 0) {
        clearInterval(countDownRef.current);
        return setTimeRemain(initTimeRemain);
      }

      let total = time_remaining;
      const hours = Math.floor(total / 3600);
      total %= 3600;
      const minutes = Math.floor(total / 60);
      const seconds = total % 60;

      // Strings with leading zeroes:
      const _hours = String(hours).padStart(2, '0');
      const _minutes = String(minutes).padStart(2, '0');
      const _seconds = Math.floor(Number(seconds)).toFixed().padStart(2, '0');

      if (time_remaining && _hours && _minutes && _seconds) {
        setTimeRemain({
          hh: _hours,
          mm: _minutes,
          ss: _seconds,
        });
      }
    }
  }, [currentLottery?.end_time, currentTimeBlockchain]);

  const _countDown = useCallback(() => {
    if (currentTimeBlockchain < 0 || !currentTimeBlockchain) return;
    setCurrentTimeBlockchain((prev) => prev + 1000);
  }, [currentTimeBlockchain]);

  const _initCountDown = useCallback(async () => {
    countDownRef.current = setInterval(_countDown, 1000);
  }, [_countDown]);

  const _initGetTimeBlockchain = useCallback(async () => {
    const current_time_blockchain = await handleGetCurrentTimeOnBlockchain();
    const current_time_to_seconds = new Date(
      (current_time_blockchain as unknown as number) / Math.pow(10, 6)
    ).getTime();
    setCurrentTimeBlockchain(current_time_to_seconds);
  }, [handleGetCurrentTimeOnBlockchain]);

  useEffect(() => {
    _initGetTimeBlockchain();
  }, [_initGetTimeBlockchain]);

  useEffect(() => {
    _initCountDown();
    return () => {
      clearInterval(countDownRef.current);
    };
  }, [_initCountDown]);

  useEffect(() => {
    _timeRemaining();
  }, [_timeRemaining]);

  return (
    <div className="wrap-current">
      <div className={'current_info orange'}>
        <h6>Current Draw #{currentLottery?.lottery_id || 0}</h6>
        <div className="current_info_value">
          <div className="clock-animated-transform-scale">
            <Image priority={true} src={clock} width={79} height={61} alt="Time remaining of current lottery" />
          </div>

          <p className="fix-width">
            <span className="space">
              {timeRemain?.hh || '00'}
              <span className="time">h</span>
            </span>
            :
            <span className="space">
              {timeRemain?.mm || '00'}
              <span className="time">m</span>
            </span>
            :
            <span className="space">
              {timeRemain?.ss || '00'}
              <span className="time">s</span>
            </span>
          </p>
        </div>
      </div>
      <h6 className="spec">
        <span className="rule-thin">Tickets purchased:</span> {currentLottery?.amount_tickets || 0}
      </h6>
    </div>
  );
};

export default NextDraw;
