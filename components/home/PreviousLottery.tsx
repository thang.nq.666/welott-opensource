/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import Image from 'next/image';
import coin_near from 'public/images/coin-near.png';
import HashLink from 'components/common/HashLink';
import { Balls } from 'components/common';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { ICurrentLottery } from 'define/interface';
import useFunctionContract from 'hooks/useFunctionContract';
import { ENV_API_URL, ENV_EXPLORER_URL, ENV_ID_TOKEN_wNEAR, tokenFormat } from 'utils/constant';
import { formatCashToView } from 'utils/common';
import useTokenPrices from 'hooks/useTokenPrices';
import axios from 'axios';

const PreviousLottery = () => {
  const { handleGetCurrentLotteryRun, handleGetLastLotteryId } = useFunctionContract();
  const { tokenPrice } = useTokenPrices({ isVisible: true });

  const [currentLottery, setCurrentLottery] = useState<ICurrentLottery>();
  const [hashString, setHashString] = useState('');

  const _getCurrentLottery = useCallback(async () => {
    const get_latest_id = await handleGetLastLotteryId();
    const prev_lottery_id = get_latest_id > 0 ? get_latest_id - 1 : get_latest_id;
    const get_current = await handleGetCurrentLotteryRun(prev_lottery_id);

    if (!get_latest_id || !get_current) return;
    setCurrentLottery({
      ...get_current,
      lottery_id: prev_lottery_id,
    });
  }, [handleGetCurrentLotteryRun, handleGetLastLotteryId]);

  const _getHashLink = useCallback(async () => {
    try {
      if (!currentLottery?.lottery_id) return;
      const hashData = await axios.get(`${ENV_API_URL}/hash?lottery_id=${currentLottery?.lottery_id}`);
      if (!hashData.data.data) return;
      setHashString(hashData.data.data.txHash);
    } catch (e) {
      console.log(e);
    }
  }, [currentLottery?.lottery_id]);

  const current_prices = useMemo(() => {
    const NEAR_DECIMALS =
      tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;
    return currentLottery?.amount_collected_in_near
      ? (currentLottery?.amount_collected_in_near as unknown as number) / Math.pow(10, NEAR_DECIMALS)
      : 0;
  }, [currentLottery?.amount_collected_in_near]);

  const current_prices_dollar = useMemo(() => {
    return current_prices * (tokenPrice?.[ENV_ID_TOKEN_wNEAR] ? tokenPrice[ENV_ID_TOKEN_wNEAR] : 1);
  }, [current_prices, tokenPrice]);

  useEffect(() => {
    _getCurrentLottery();
  }, [_getCurrentLottery]);

  useEffect(() => {
    _getHashLink();
  }, [_getHashLink]);

  return (
    <div className="wrap-previous-lottery">
      <div className="section">
        <h6 className="title">Previous Round</h6>
        <h6 className="value">#{currentLottery?.lottery_id ? currentLottery?.lottery_id : 0}</h6>
      </div>
      <div style={{ position: 'relative' }} className="winning-card-chain">
        <div className="section mid">
          <h6 className="title">Winning number</h6>
          <div className="value">
            {currentLottery?.final_number ? (
              currentLottery?.final_number
                ?.toString()
                .split('')
                .slice(1)
                .map((win_num, idx) => (
                  <span key={idx} className="number" style={{ height: 56 }}>
                    <Balls
                      ballNumber={Number(win_num)}
                      width={32}
                      height={32}
                      customStyle={{
                        boxShadow:
                          'rgb(0 0 0 / 17%) 0px -23px 25px 0px inset, rgb(0 0 0 / 15%) 0px -36px 30px 0px inset, rgb(0 0 0 / 10%) 0px -79px 40px 0px inset, rgb(0 0 0 / 6%) 0px 2px 1px, rgb(0 0 0 / 9%) 0px 4px 2px, rgb(0 0 0 / 9%) 0px 8px 4px, rgb(0 0 0 / 9%) 0px 16px 8px, rgb(0 0 0 / 9%) 0px 32px 16px',
                      }}
                    />
                  </span>
                ))
            ) : (
              <h2>Not Available</h2>
            )}
          </div>
        </div>
        <div className="hash-position">
          <HashLink url={`${ENV_EXPLORER_URL}/transactions/${hashString}`} />
        </div>
      </div>
      <div className="section">
        <h6 className="title">Pot Sizes</h6>
        <div className="value">
          <span className="icon">
            <Image priority={true} src={coin_near} width={40} height={40} alt="Coin Near" />
          </span>
          {formatCashToView(Number(current_prices), 8)}
        </div>
        <p className="spec cl-yellow on-desktop">
          {`${tokenPrice ? '~$' + formatCashToView(Number(current_prices_dollar), 8) : 'Calculating...'}`}
        </p>
      </div>
    </div>
  );
};

export default PreviousLottery;
