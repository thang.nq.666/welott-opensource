/* eslint-disable @typescript-eslint/no-explicit-any */
import { Balls } from 'components/common';
import { StateContext } from 'context/StateContext';
import { EAction, ELotteryStatus, EModal } from 'define/enum';
import { ICurrentLottery, IRoundUserInfo } from 'define/interface';
import useActionValidate from 'hooks/useActionValidate';
import useFunctionContract from 'hooks/useFunctionContract';
import Image from 'next/image';
import clock from 'public/images/clock.png';
import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import styled, { css, keyframes } from 'styled-components';
import { formatCashToView } from 'utils/common';
import { ENV_ID_TOKEN_wNEAR, tokenFormat } from 'utils/constant';

const initTimeRemain = {
  hh: '00',
  mm: '00',
  ss: '00',
};

const HistoryBalls = ({ final_number, current_lottery_id }: { final_number: string; current_lottery_id: string }) => {
  const countDownRef = useRef<any>();

  const { handleGetLastLotteryId, handleGetCurrentLotteryRun, handleGetCurrentTimeOnBlockchain } =
    useFunctionContract();
  const [currentLottery, setCurrentLottery] = useState<ICurrentLottery>();
  const [currentTimeBlockchain, setCurrentTimeBlockchain] = useState(0);

  const [timeRemain, setTimeRemain] = useState(initTimeRemain);

  const getCurrentLottery = useCallback(async () => {
    const last_id = await handleGetLastLotteryId();

    if (Number(current_lottery_id) !== last_id) return;

    const get_current = await handleGetCurrentLotteryRun(last_id);
    setCurrentLottery({
      ...get_current,
      lottery_id: last_id,
    });
  }, [current_lottery_id, handleGetCurrentLotteryRun, handleGetLastLotteryId]);

  const _timeRemaining = useCallback(async () => {
    const end_time = new Date((currentLottery?.end_time as unknown as number) / Math.pow(10, 6)).getTime();

    if (currentTimeBlockchain) {
      const time_remaining = (end_time - currentTimeBlockchain) / 1000;
      if (time_remaining < 0) {
        clearInterval(countDownRef.current);
        return setTimeRemain(initTimeRemain);
      }

      let total = time_remaining;
      const hours = Math.floor(total / 3600);
      total %= 3600;
      const minutes = Math.floor(total / 60);
      const seconds = total % 60;

      // Strings with leading zeroes:
      const _hours = String(hours).padStart(2, '0');
      const _minutes = String(minutes).padStart(2, '0');
      const _seconds = Math.floor(Number(seconds)).toFixed().padStart(2, '0');

      if (time_remaining && _hours && _minutes && _seconds) {
        setTimeRemain({
          hh: _hours,
          mm: _minutes,
          ss: _seconds,
        });
      }
    }
  }, [currentLottery?.end_time, currentTimeBlockchain]);

  const _countDown = useCallback(() => {
    if (currentTimeBlockchain < 0 || !currentTimeBlockchain) return;
    setCurrentTimeBlockchain((prev) => prev + 1000);
  }, [currentTimeBlockchain]);

  const _initCountDown = useCallback(async () => {
    countDownRef.current = setInterval(_countDown, 1000);
  }, [_countDown]);

  const _initGetTimeBlockchain = useCallback(async () => {
    const current_time_blockchain = await handleGetCurrentTimeOnBlockchain();
    const current_time_to_seconds = new Date(
      (current_time_blockchain as unknown as number) / Math.pow(10, 6)
    ).getTime();
    setCurrentTimeBlockchain(current_time_to_seconds);
  }, [handleGetCurrentTimeOnBlockchain]);

  useEffect(() => {
    getCurrentLottery();
  }, [getCurrentLottery]);

  useEffect(() => {
    _initGetTimeBlockchain();
  }, [_initGetTimeBlockchain]);

  useEffect(() => {
    _timeRemaining();
  }, [_timeRemaining]);

  useEffect(() => {
    if (currentLottery?.lottery_id) {
      _initCountDown();
    }
    return () => {
      clearInterval(countDownRef.current);
    };
  }, [_initCountDown, currentLottery?.lottery_id]);

  return final_number ? (
    <React.Fragment>
      {final_number
        .toString()
        .split('')
        .slice(1)
        ?.map((win_num, idx) => (
          <BallAnimate
            key={idx}
            style={{
              animationDelay: `${idx / 10}s`,
            }}
          >
            <Balls ballNumber={Number(win_num)} width={32} height={32} />
          </BallAnimate>
        ))}
    </React.Fragment>
  ) : (
    <div className="current_info_value current_info_value_history_item">
      <div className="clock-animated-transform-scale">
        <Image priority={true} src={clock} width={50} height={37.5} alt="Time remaining of current lottery" />
      </div>

      <p className="fix-width">
        <span className="space">
          {timeRemain?.hh || '00'}
          <span className="time">h</span>
        </span>
        :
        <span className="space">
          {timeRemain?.mm || '00'}
          <span className="time">m</span>
        </span>
        :
        <span className="space">
          {timeRemain?.ss || '00'}
          <span className="time">s</span>
        </span>
      </p>
    </div>
  );
};

const HistoryItem = ({ item }: { item: IRoundUserInfo }) => {
  const { dispatch, roundUserInfo } = useContext(StateContext);
  const { handleOpenModal } = useActionValidate();
  const [currentUserInfo, setCurrentUserInfo] = useState<IRoundUserInfo>();

  const _toggleLotteryDetailModal = () => {
    handleOpenModal(EModal.DETAIL);
    dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: Number(item.id) });
  };

  const _onToggleClaim = (id: number) => {
    if (!id) return;
    dispatch({
      type: EAction.SET_CURRENT_LOTTERY_ID,
      payload: id,
    });
    handleOpenModal(EModal.CLAIM);
  };

  const current_prices = useMemo(() => {
    const NEAR_DECIMALS =
      tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;
    return (item?.amount_collected_in_near as unknown as number) / Math.pow(10, NEAR_DECIMALS);
  }, [item?.amount_collected_in_near]);

  const _percent_of = useCallback((number: number, percent: number) => number * (percent / 10000), []);
  const cal_treasury_fee = current_prices - current_prices * (Number(currentUserInfo?.treasury_fee) / 10000);
  const _isWinningCurrentRound = currentUserInfo?.near_in_rewards.find(
    (lott) => lott.status === ELotteryStatus.CLAIMABLE
  );

  const _initCurrentUserInfo = useCallback(() => {
    if (!roundUserInfo) return;
    const current = roundUserInfo.find((lott) => Number(lott.id) === Number(item.id));
    setCurrentUserInfo(current);
  }, [item.id, roundUserInfo]);

  useEffect(() => {
    _initCurrentUserInfo();
  }, [_initCurrentUserInfo]);

  return (
    <>
      <div className="wrap-title wrap-title-value">
        <div className="block-title round">
          <p className="on-mobile">Round</p>
          <p>#{item.id}</p>
        </div>
        <div className="block-title win-numbers">
          <p className="on-mobile">Winning numbers</p>
          <div className="flex-win-numbers">
            <HistoryBalls current_lottery_id={item.id} final_number={item.final_number} />
          </div>
        </div>
        <div className="block-title pot-sizes">
          <p className="on-mobile">Pot size ($NEAR)</p>
          <div className="value-item">
            <p className="top">
              {formatCashToView(Number(currentUserInfo?.amount_collected_in_near) / Math.pow(10, 24), 8)}{' '}
              <span className="on-mobile">NEAR</span>
            </p>
            {/* <p className="bot on-desktop cl-base-gray-2">${formatCashToView(current_prices_dollar(current_prices), 8)}</p> */}
          </div>
        </div>
        {item.rewards_breakdown
          ? item.rewards_breakdown.split(',').map((reward_percent, idx) => (
              <div key={reward_percent} className="block-title prize prize-1">
                <p className="on-mobile">#{idx + 1}</p>
                <div className="value-item">
                  <p className="top">
                    {formatCashToView(_percent_of(cal_treasury_fee, Number(reward_percent)), 8)}{' '}
                    <span className="on-mobile">NEAR</span>
                  </p>
                </div>
              </div>
            ))
          : [1, 2, 3, 4, 5, 6].map((item, idx) => (
              <div key={item} className="block-title prize prize-1">
                <p className="on-mobile">#{idx + 1}</p>
                <div className="value-item">
                  <p className="top">
                    0.00 <span className="on-mobile">NEAR</span>
                  </p>
                </div>
              </div>
            ))}
        <div className="block-title your-tickets">
          <p className="on-mobile">Your tickets</p>
          <div className="value-button">
            <button
              className={`common-button ${_isWinningCurrentRound ? 'yellow' : 'gray'} ${
                !item.final_number ? 'btn-disable' : ''
              }`}
              onClick={() => _onToggleClaim(Number(item.id))}
            >
              {currentUserInfo?.tickets_len || 0}
            </button>
            <button onClick={_toggleLotteryDetailModal} className="common-button gray">
              Details
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default HistoryItem;

// fake rule:
// status 0 = View All
// status 1 = Claim
// status 2 = Claimed
// status 3 = 0->X ticket - to check the last ticket has buy

const BallAnimate = styled.div`
  position: relative;
  animation: ${() =>
    css`
      ${animatebott} .5s
    `};
  animation-fill-mode: backwards;
`;
const animatebott = keyframes`
from {
    left: -50px;
    transform: rotate(0deg);
  }
to {
    left: 0;
    transform: rotate(360deg);
  }
`;
