import Image from 'next/image';
import rule_board from 'public/images/rule-board.png';
import MiniChart from 'public/images/mini-chart.svg';

const HowToPlay = () => {
  return (
    <div id="how-to-play" className="how-to-play">
      <h3 className="big-title">How to play</h3>
      <div className="wrap-step" style={{ width: '100%' }}>
        <div className="wrap-step" style={{ width: '70%' }}>
          <div className="step">
            <h5 className="title cl-f-light-green">Step 1</h5>
            <h6 className="sub-title">Buy tickets</h6>
            <p className="text">Select number and confirm payment.</p>
          </div>
          <div className="step">
            <h5 className="title cl-f-light-green">Step 2</h5>
            <h6 className="sub-title">Wait for the Draw</h6>
            <p className="text">NEAR VRF will be used to select winning numbers at the time of drawing.</p>
          </div>
        </div>
        <div className="step">
          <h5 className="title cl-f-light-green">Step 3</h5>
          <h6 className="sub-title">Check for Prizes</h6>
          <p className="text">After the round is over, go back and check the results!</p>
        </div>
      </div>
      <div className="winning-rule-section">
        <div className="item-flex criteria">
          <h6 className="title">Winning Criteria</h6>
          <p className="desc cl-f-light-green mt-8 mb-8">Winning Ticket:</p>
          <p className="text">
            The digits on ticket must line up in the proper sequence. Example lottery draw with two tickets:
          </p>
          <ul>
            <li>
              <span className="hight-2-light">Ticket A:</span> The last three digits and the first two digits match, but
              the 3rd digit is wrong, so this ticket only wins a “Match last 3 numbers” prize.
            </li>
            <li>
              <span className="hight-2-light">Ticket B:</span> Even though the first five digits match, the last digit
              is wrong, so this ticket doesn&apos;t win a prize.
            </li>
          </ul>
          <p className="text">
            Prize brackets don&apos;t &apos;stack&apos;: if you match the last three digits in order, you&apos;ll only
            win prizes from the &apos;Match 3&apos; bracket and not from &apos;Match last number&apos; and &apos;Match
            last 2 numbers&apos;.
          </p>
          <p className="desc cl-f-light-green mt-16 mb-8">Method of choosing winning numbers: </p>
          <p className="text">
            Welott integrates with{' '}
            <a className="linking-des" href="https://docs.rs/near-sdk/latest/near_sdk/env/fn.random_seed.html#">
              NEAR VRF
            </a>{' '}
            link (Verifiable Random Function) to choose the winning number. Initially, NEAR VRF is a provably fair and
            verifiable random number generator (RNG) that enables smart contracts to access random values without
            compromising security or usability.
          </p>
          <ul className="method-choosing">
            <li>
              <span className="hight-2-light">Step 1:</span> Pick 10 first numbers from the VRF number.
            </li>
            <li>
              <span className="hight-2-light">Step 2:</span> The 10 selected numbers will combine into a string and
              convert to a number. We can call the name is fn1
            </li>
            <li>
              <span className="hight-2-light">Step 3:</span> In the last step, Welott will select the final number by
              using the math: (1,000,000 + (fn1 modulo 1,000,000))
            </li>
          </ul>
        </div>

        <div
          className="item-flex rule-board"
          style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}
        >
          <div className="wrap-img-rule-board">
            <Image priority={true} alt="Mountains" src={rule_board} layout="fill" objectFit="contain" />
          </div>
          <div className="wrap-game-rule">
            <p className=" hight-2-light mb-8 desc">For example</p>
            <div className="wrapp-game-rule-content">
              <div className="rule-mutual">
                <span>The NEAR VRF Number </span>
                <br />
                <span className="rule-break-word">[51,4,206,36,21,157,82,7,78,20,11,...]</span>
              </div>
              <div className="rule-mutual">
                <span>
                  <span className="hight-2-light">Step 1: </span> <span>First 10 numbers</span>
                </span>{' '}
                <span>[51,4,206,36,21,157,82,7,78,20]</span>
              </div>
              <div className="rule-mutual">
                <span className="hight-2-light">Step 2: </span> <span>Generated number</span>{' '}
                <span>51420636211578277820</span>
              </div>
              <div className="rule-mutual">
                <span className="hight-2-light">Step 3: </span> <span>The winning number</span> <br />
                <span className="hight-2-light mb-8 desc rule-display-block rule-text-center winning-number">
                  2 7 7 8 2 0
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="winning-rule-section">
        <div className="item-flex criteria">
          <h6 className="title">Prize Funds</h6>

          <p className="text mt-16 mb-16">The prizes for each round come from two sources: </p>
          <p className="desc cl-f-light-green mt-16">Ticket Purchases</p>
          <p className="text mb-16">
            95% of the NEAR paid by ticket buyers in that round goes directly into the prize pools
          </p>

          <p className="desc cl-f-light-green mt-16">Rollover Prizes</p>
          <p className="text mb-16">
            After every round, if nobody wins in one of the prize brackets, the unclaimed NEAR for that bracket rolls
            over into the next round and are redistributed among the prize pools.
          </p>

          <p className="desc cl-f-light-green mt-16">Addional Notes</p>
          <p className="text mb-16">
            Prizes for each category will be divided equally among the number of winning tickets.
          </p>

          <p className="text mt-16 mb-16">
            {`All 5% of the ticket fees will be donated to the "Unchain Fund" for charity purposes.`}
          </p>
        </div>

        <div className="item-flex rule-board">
          <div className="mini-chart">
            <MiniChart />
          </div>
          <div className="wrap-info">
            <div className="info">
              <div className="side">DIGITS MATCHED</div>
              <div className="fwb">Allocation</div>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#EDD104' }}></div>Last number
              </div>
              <p className="fwb">1%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#61AD00' }}></div>Last 02 numbers
              </div>
              <p className="fwb">2%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#007521' }}></div>Last 03 numbers
              </div>
              <p className="fwb">5%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#02E7F6' }}></div>Last 04 numbers
              </div>
              <p className="fwb">10%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#5002F6' }}></div>Last 05 numbers
              </div>
              <p className="fwb">20%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#D402F6' }}></div>Last 06 numbers
              </div>
              <p className="fwb">40%</p>
            </div>
            <div className="info">
              <div className="side">
                <div className="dot" style={{ background: '#808080' }}></div>Reserve Pool
              </div>
              <p className="fwb">22%</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowToPlay;
