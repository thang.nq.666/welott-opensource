import React from 'react';
import Link from 'next/link';

import Image from 'next/image';
import styled from 'styled-components';

const Footer = () => {
  return (
    <Wrapper>
      <SocialWrapper>
        {IconList.map((item, idx) => (
          <Link href={item.to} target="_blank" key={idx}>
            <a target="_blank">
              <Image src={item.src} alt="social" width={25} height={25} />
            </a>
          </Link>
        ))}
      </SocialWrapper>
    </Wrapper>
  );
};

export default Footer;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 100px 0 20px 0;
`;

const SocialWrapper = styled.ul`
  display: flex;
  gap: 40px;
  list-style: none;
  padding-left: 0;
  a {
    cursor: pointer;
    transition: all 0.3s ease-in-out;
    opacity: 0.7;
    &:hover {
      transform: translateY(-3px);
      opacity: 1;
    }
  }
`;

const IconList = [
  {
    to: 'https://twitter.com/NearlendDao',
    src: '/images/twiter.svg',
  },
  {
    to: 'https://t.me/+gYhnDfknkRdhMjQ1',
    src: '/images/telegram.svg',
  },
  {
    to: 'https://discord.gg/pXvHhT9rwM',
    src: '/images/discord.svg',
  },
  {
    to: 'https://medium.com/@Nearlend_DAO',
    src: '/images/medium.svg',
  },
  {
    src: '/images/github.svg',
    to: 'https://github.com/nearlend-dao/welott-contract',
  },
];
