/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { InputNumber, Tabs } from 'antd';
import WrapModal from './WrapModal';
import { StateContext } from 'context/StateContext';
import { EModal } from 'define/enum';
import { CaretDownOutlined, CaretUpOutlined, DeleteOutlined } from '@ant-design/icons';
import useFunctionContract from 'hooks/useFunctionContract';
import { formatCashToView } from 'utils/common';
import { ENV_ID_TOKEN_wNEAR, tokenFormat } from 'utils/constant';
import { ICurrentLottery } from 'define/interface';

import { InfoCircleFilled } from '@ant-design/icons';
import { Tooltip } from 'antd';
import IconManual from 'public/images/manual.svg';
import IconShuffle from 'public/images/shuffle.svg';
import { welottText } from 'utils/text';
interface IProps {
  title?: string;
}
const list_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
const initLengthNumberChoose = Array.from({ length: 6 });
const max_random = 12;
const NEAR_DECIMALS =
  tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;

const BuyTicketModal = ({ title = 'Buy Ticket' }: IProps) => {
  const { nameOpenModal, userNearBalance } = useContext(StateContext);
  const {
    handleGetTotalPriceTickets,
    handleGetLastLotteryId,
    handleBuyTicket,
    handleGetCurrentLotteryRun,
    handleGetFullUserTicketNumber,
  } = useFunctionContract();

  const visible = useMemo(() => (nameOpenModal === EModal.BUY_TICKET ? true : false), [nameOpenModal]);
  const [numbersChoosing, setNumberChoosing] = useState<string[]>([]);
  const [listNumberChoose, setListNumberChoose] = useState<string[]>([]);
  const [countRandom, setCountRandom] = useState(0);
  const [inputRandom, setInputRandom] = useState<number>(0);
  const [ticketPrice, setTicketPrice] = useState(0);
  const [total, setTotal] = useState('0');
  const [lastId, setLastId] = useState(1);
  const [ticketNumbers, setTicketNumbers] = useState<number[]>([]);

  const _handleSetNumberChoose = useCallback(
    (item: string) => {
      if (numbersChoosing.length >= 6) return;
      setNumberChoosing([...numbersChoosing, item]);
    },
    [numbersChoosing]
  );

  const _handleClearNumberChoose = () => {
    setNumberChoosing([]);
  };
  const _checkDisableAdd = useMemo(() => {
    if (ticketNumbers.length >= 120 || numbersChoosing.length < 6 || listNumberChoose.length === 12) return true;
    return false;
  }, [listNumberChoose.length, numbersChoosing.length, ticketNumbers.length]);

  const _checkDisableBuy = useMemo(() => {
    if (ticketNumbers.length >= 120 || listNumberChoose.length < 1) return true;
    return false;
  }, [listNumberChoose.length, ticketNumbers.length]);

  const _handleAddSingleLott = useCallback(() => {
    if (_checkDisableAdd) return;
    const mod_number_choose = numbersChoosing.join('');
    if (!mod_number_choose || mod_number_choose.length < 6) return;
    setListNumberChoose([...listNumberChoose, mod_number_choose]);
    setNumberChoosing([]);
  }, [_checkDisableAdd, listNumberChoose, numbersChoosing]);

  const _handleDeleteChooseNumber = (key: number) => {
    setListNumberChoose((prev) => prev.filter((_, idx) => idx !== key));
  };

  const _handleBuyTicket = async function () {
    try {
      if (_checkDisableBuy) return;
      const listNumberChoose_mod = listNumberChoose.map((item) => 1000000 + Number(item));
      return await handleBuyTicket(lastId, listNumberChoose_mod, total);
    } catch (error) {
      console.log(error);
    }
  };

  const _addRandom = useCallback(
    (amount: number) => {
      const to_number_listNumberChoose = listNumberChoose.map((item) => Number(item));
      const numbers = [...to_number_listNumberChoose]; // new empty or exist array
      const max = 999999;
      const round = amount; // how many numbers you want to extract
      let curr_number = 0;
      let isExist = false;
      for (let i = 0; i < round; i++) {
        do {
          curr_number = Math.floor(Math.random() * max + 1);
          isExist = numbers.includes(curr_number);
          if (!isExist) {
            numbers.push(curr_number);
          }
        } while (isExist);
      }
      const numbers_mod = numbers.map((num) => (1000000 + num).toString().slice(1));
      setListNumberChoose(numbers_mod);
    },
    [listNumberChoose]
  );

  const _handleMaxRandom = () => {
    setInputRandom(countRandom);
  };

  const _onChangeInputRandom = (e: number) => {
    //prevent User input float number
    const e_mod = parseInt(e.toFixed(2)) as unknown as number;
    if (e_mod <= countRandom) setInputRandom(e_mod);
    if (e_mod > countRandom) setInputRandom(countRandom);
  };

  const _addInputNumber = useCallback(() => {
    const inputRandom_f = inputRandom as unknown as number;
    if (inputRandom_f < 0 || inputRandom_f > countRandom) return;
    _addRandom(inputRandom_f);
    setInputRandom(0);
  }, [_addRandom, countRandom, inputRandom]);

  const _onFocus = (e: any) => {
    // clean 0 number fist-time onFocus
    if (e.target.value === '0') return setInputRandom(e);
  };

  const _increase = () => {
    if (inputRandom >= countRandom) return;
    setInputRandom((prev) => prev + 1);
  };

  const _decrease = () => {
    if (inputRandom === 0) return;
    setInputRandom((prev) => prev - 1);
  };

  const _autoGetPrice = useCallback(async () => {
    const totalPriceTickets = (await handleGetTotalPriceTickets(lastId, listNumberChoose.length)) as unknown as string;
    setTotal(totalPriceTickets);
  }, [handleGetTotalPriceTickets, lastId, listNumberChoose.length]);

  const _initPricePerTicket = useCallback(async () => {
    const current_lottery = (await handleGetCurrentLotteryRun(lastId)) as unknown as ICurrentLottery;
    const ticket_in_near = current_lottery?.price_ticket_in_near || 0;

    setTicketPrice(ticket_in_near);
  }, [handleGetCurrentLotteryRun, lastId]);

  const _initLastId = useCallback(async () => {
    const lastLotteryId = (await handleGetLastLotteryId()) as unknown as number;
    setLastId(lastLotteryId);
  }, [handleGetLastLotteryId]);

  const _getUserTicketNumber = useCallback(async () => {
    const result = await handleGetFullUserTicketNumber(lastId);
    if (!result) return;
    setTicketNumbers(result);
  }, [handleGetFullUserTicketNumber, lastId]);

  const _resetState = useCallback(() => {
    if (visible) return;
    setListNumberChoose([]);
    setNumberChoosing([]);
  }, [visible]);

  useEffect(() => {
    _getUserTicketNumber();
  }, [_getUserTicketNumber]);

  useEffect(() => {
    _resetState();
  }, [_resetState]);

  useEffect(() => {
    const user_balance = Number(formatCashToView(Number(userNearBalance.available), 8));
    const amout_ticket_can_buy = Math.floor(
      user_balance / Number(formatCashToView(ticketPrice / Math.pow(10, NEAR_DECIMALS), 8))
    );
    const valid_max = amout_ticket_can_buy < max_random ? amout_ticket_can_buy : max_random;
    setCountRandom(valid_max - listNumberChoose.length);
  }, [listNumberChoose.length, ticketPrice, userNearBalance.available]);

  useEffect(() => {
    _initPricePerTicket();
  }, [_initPricePerTicket]);

  useEffect(() => {
    _autoGetPrice();
  }, [_autoGetPrice]);

  useEffect(() => {
    _initLastId();
  }, [_initLastId]);

  const handleKeyDown = useCallback(
    (event: any) => {
      if (event.key === 'Enter') {
        return _handleAddSingleLott();
      }
      if (event.key === 'Backspace') {
        return _handleClearNumberChoose();
      }
      if (!/[0-9]/.test(event.key)) {
        return event.preventDefault();
      }
      if (numbersChoosing.length >= 6) return;
      _handleSetNumberChoose(event.key.toString());
    },
    [_handleAddSingleLott, _handleSetNumberChoose, numbersChoosing.length]
  );

  useEffect(() => {
    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [handleKeyDown]);

  return (
    <WrapModal
      className="buy-ticket-modal"
      visible={visible}
      title={
        <div className="title">
          <span className="ant-modal-title">{title}</span>{' '}
          <Tooltip arrowPointAtCenter color="#0000006b" title={welottText.max_12} placement="bottom">
            <InfoCircleFilled />
          </Tooltip>
        </div>
      }
    >
      <div>
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane
            key={'1'}
            tab={
              <span className="tab-content">
                <IconManual />
                Manual Choice
              </span>
            }
          >
            <div className="wrap-number-pad">
              <h4>Chosen 6 numbers</h4>
              <div className="number-pad">
                {list_numbers.map((item, idx) => (
                  <button key={idx} onClick={() => _handleSetNumberChoose(item.toString())}>
                    {item}
                  </button>
                ))}
              </div>
            </div>
            <div className="you-number">
              <div className="top">
                <h5>You Number</h5>
                <h5 className="clear" onClick={_handleClearNumberChoose}>
                  <DeleteOutlined />
                  Clear
                </h5>
              </div>
              <div className="control">
                <div className="number-chosen">
                  {initLengthNumberChoose.map((_, index) => (
                    <div key={index} className="number-circle">
                      <p>{numbersChoosing[index] ? numbersChoosing[index] : '_'}</p>
                    </div>
                  ))}
                </div>
                <button
                  className={`add-button ${_checkDisableAdd ? 'btn-disable' : ''}`}
                  onClick={_handleAddSingleLott}
                >
                  Add
                </button>
              </div>
            </div>
          </Tabs.TabPane>
          <Tabs.TabPane
            key={'2'}
            tab={
              <span className="tab-content">
                <IconShuffle />
                Random Choice
              </span>
            }
          >
            <div className="wrap-random">
              <p className="mt-8 cl-smoke-gray">Enter the number of tickets you want to buy</p>
              <div className="action-random">
                <div className="wrap-input">
                  <CaretDownOutlined onClick={_decrease} className="btn-input btn-decrease" />
                  <InputNumber
                    type={'number'}
                    className={'input-custom'}
                    value={inputRandom ?? null}
                    onFocus={_onFocus}
                    onChange={(e) => _onChangeInputRandom(e || 0)}
                  />
                  <CaretUpOutlined onClick={_increase} className="btn-input btn-increase" />
                  <p className="cl-smoke-gray pl">ticket(s)</p>
                </div>
                <button
                  onClick={_handleMaxRandom}
                  className={`common-button yellow ${!countRandom ? 'btn-disable' : ''}`}
                >
                  Max ({countRandom})
                </button>
              </div>
              <button
                onClick={_addInputNumber}
                className={`common-button add-random-btn ${!countRandom ? 'btn-disable' : ''}`}
              >
                Add
              </button>
            </div>
          </Tabs.TabPane>
        </Tabs>
        <div className="wrap-combination">
          <h6 className="title">Combination waiting to be submitted</h6>
          <div className="list-number-choose">
            {listNumberChoose.map((number, index) => (
              <div key={index} className="wrap-number-choose">
                <div className="number-show">
                  {number
                    .toString()
                    .split('')
                    .map((i, idx) => (
                      <div key={idx} className="number-circle">
                        <p>{i}</p>
                      </div>
                    ))}
                </div>
                <div className="number-del" onClick={() => _handleDeleteChooseNumber(index)}>
                  <DeleteOutlined />
                </div>
              </div>
            ))}
          </div>
          <div className="buy-info">
            <p>Balance</p>
            <p>{formatCashToView(Number(userNearBalance.available), 8)} $NEAR</p>
          </div>
          <div className="buy-info">
            <p>
              Total ticket{' '}
              <Tooltip
                className="title-tooltip"
                zIndex={109999}
                arrowPointAtCenter
                color="#0000006b"
                title={`${formatCashToView(ticketPrice / Math.pow(10, NEAR_DECIMALS), 8)} $NEAR/ticket`}
              >
                <InfoCircleFilled />
              </Tooltip>
              {/* <span className="on-desktop">({ticketPrice / Math.pow(10, NEAR_DECIMALS)} NEAR/ticket)</span> */}
            </p>
            <p>{listNumberChoose.length} Ticket</p>
          </div>
          <div className="buy-info">
            <p className="cl-green-lott">Total Fees</p>
            <p className="cl-green-lott">{formatCashToView(Number(total) / Math.pow(10, NEAR_DECIMALS), 8)} $NEAR</p>
          </div>
          <button
            onClick={_handleBuyTicket}
            className={`common-button modal-buy-btn ${_checkDisableBuy && 'btn-disable'}`}
          >
            Buy
          </button>
        </div>
      </div>
    </WrapModal>
  );
};

export default BuyTicketModal;
