/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import WrapModal from './WrapModal';
import { StateContext } from 'context/StateContext';
import { EAction, ELotteryStatus, EModal } from 'define/enum';
import styled from 'styled-components';
import Image from 'next/image';
import coin_near from 'public/images/coin-near.png';
import useFunctionContract from 'hooks/useFunctionContract';
import { Select } from 'antd';
import { formatCashToView, matchingNumbers, getRewardByTicket } from 'utils/common';
import { IRoundUserInfo } from 'define/interface';

const { Option } = Select;
const { CLAIMABLE } = ELotteryStatus;

interface IUserWinningTicket {
  ticket_id: number[];
  ticket_number: number[];
  highest_bracket: number[];
}

interface ICurrentRecord {
  ticket: number;
  final: number;
  match: { status: boolean; number: string }[];
}

interface IProps {
  title?: string;
}

const ClaimTicketModal = ({ title = 'Your Winning Tickets' }: IProps) => {
  const { nameOpenModal, currentLotteryId, dispatch, roundUserInfo, userStatistic } = useContext(StateContext);
  const { handleClaimInRound } = useFunctionContract();

  const visible = useMemo(() => nameOpenModal === EModal.CLAIM, [nameOpenModal]);
  const [currentRecord, setCurrentRecord] = useState<ICurrentRecord[]>();
  const [currentLottery, setCurrentLottery] = useState<IRoundUserInfo>();
  const [currentUserTicket, setCurrentUserTicket] = useState<IRoundUserInfo>();

  const _initNearPerBracket = useCallback(async () => {
    const curr = roundUserInfo.find((lott) => Number(lott.id) === currentLotteryId);
    if (!curr) return;
    setCurrentLottery(curr);
  }, [currentLotteryId, roundUserInfo]);

  const _initModalRecordState = useCallback(async () => {
    const curr = roundUserInfo.find((lott) => Number(lott.id) === currentLotteryId);
    if (!curr) return;

    const sort_near_in_rewards = curr.near_in_rewards.sort((a, b) => b.rewards - a.rewards);
    setCurrentUserTicket({ ...curr, near_in_rewards: sort_near_in_rewards });
    const ticket_numbers = sort_near_in_rewards.map((lott) => Number(lott.ticket_number));
    const result_matching = matchingNumbers(Number(curr.final_number), ticket_numbers);
    setCurrentRecord(result_matching);
  }, [currentLotteryId, roundUserInfo]);

  const _handleSelectRound = useCallback(
    (round: number) => {
      dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: round });
    },
    [dispatch]
  );

  const isClaimable = currentUserTicket?.near_in_rewards.find((lott) => Number(lott.status) === CLAIMABLE);

  const _handleClaimInRound = () => {
    if (!isClaimable) return;

    const newUserTicket = currentUserTicket?.near_in_rewards.reduce(
      (prev: IUserWinningTicket, curr, currIndex) => {
        const ticket_status = curr.status;
        const { rewards, highest_bracket } = getRewardByTicket(
          currentLottery?.final_number as unknown as number,
          Number(curr.ticket_number),
          currentLottery?.near_per_bracket as unknown as number[]
        );
        if (ticket_status === CLAIMABLE && rewards > 0) {
          prev = {
            ticket_id: [...prev.ticket_id, Number(currentUserTicket.near_in_rewards[currIndex].ticket_id)],
            ticket_number: [...prev.ticket_number, Number(currentUserTicket.near_in_rewards[currIndex].ticket_number)],
            highest_bracket: [...prev.highest_bracket, highest_bracket],
          };
        }

        return prev;
      },
      {
        ticket_number: [],
        highest_bracket: [],
        ticket_id: [],
      }
    );

    return handleClaimInRound(
      currentLotteryId,
      newUserTicket?.ticket_id as unknown as number[],
      newUserTicket?.highest_bracket as unknown as number[]
    );
  };

  useEffect(() => {
    _initModalRecordState();
  }, [_initModalRecordState]);

  useEffect(() => {
    _initNearPerBracket();
  }, [_initNearPerBracket]);

  return (
    <div id="test-modal" className="test-modal">
      <ClaimWrapModal>
        <WrapModal visible={visible} title={<ModalTitleHead>{title}</ModalTitleHead>} className="claim-modal-history">
          <div id="winning-claim-token-modal">
            <ClaimTokenBanner>
              <div className="claim-banner-wrapper">
                <div className="claim-banner-logo">
                  <Image priority={true} src={coin_near} width={55} height={55} alt="Near coin" />
                </div>
                <div className="claim-banner-mid-content">
                  <div className="claim-banner-winning-round">
                    <span className="claim-banner-winning-round-title">UNCLAIMED round: </span>{' '}
                    <span>
                      {userStatistic.lotteries_won?.length
                        ? userStatistic.lotteries_won.map((item) => `#${item}`).join(', ')
                        : '-'}
                    </span>
                  </div>
                  <div className="claim-banner-winning-total">
                    <span className="claim-banner-winning-total-title">Total Winning:</span>
                    <span className="claim-banner-winning-total-mid-value">
                      {`${
                        userStatistic.near_amount_can_claim
                          ? formatCashToView(userStatistic.near_amount_can_claim, 10)
                          : 0
                      } $NEAR`}
                    </span>
                  </div>
                </div>
              </div>
            </ClaimTokenBanner>
            <ClaimTokenBody>
              <div className="flex">
                <SelectWrapper value={`Round ${currentLotteryId}`} defaultValue={`Round 1`}>
                  {userStatistic.lotteries_joined.reverse().map((round, idx) => (
                    <Option key={idx} value={Number(round)} isSelectOption={idx === currentLotteryId ? true : false}>
                      <p onClick={() => _handleSelectRound(Number(round))}>Round {round}</p>
                    </Option>
                  ))}
                </SelectWrapper>
                <div className="claim-banner-button">
                  <div
                    className={`claim-btn ${isClaimable ? 'claim-btn-active' : 'claim-btn-disable'}`}
                    onClick={_handleClaimInRound}
                  >
                    Claim
                  </div>
                </div>
              </div>
              <ClaimListView>
                {currentRecord?.length ? (
                  <div className="claim-record">
                    {currentRecord?.map((curr, idx: number) => {
                      const matchItem = curr?.match;
                      const rewards = currentUserTicket?.near_in_rewards[idx].rewards || 0;
                      return (
                        <div key={idx} className="item-claim-record">
                          <div>
                            <span className="span-flex">
                              {matchItem?.map((i: { status: boolean; number: string }, j: number) => {
                                return (
                                  <CircleNumber
                                    key={j}
                                    className={`
                                      reward-item-default 
                                      animate-toBottom 
                                      ${i.status ? 'reward-item-active' : ''}`}
                                    style={{
                                      animationDelay: `${j / 30}s`,
                                    }}
                                  >
                                    <span>{i?.number}</span>
                                  </CircleNumber>
                                );
                              })}
                            </span>
                          </div>
                          <ClaimAmount>
                            <span>{formatCashToView(rewards)}</span> <span>$NEAR</span>
                          </ClaimAmount>
                        </div>
                      );
                    })}
                  </div>
                ) : (
                  <NoTicket>
                    {currentLottery?.final_number
                      ? 'You have no ticket at this round !'
                      : 'Please wait until the end of current draw!'}
                  </NoTicket>
                )}
              </ClaimListView>
            </ClaimTokenBody>
          </div>
        </WrapModal>
      </ClaimWrapModal>
    </div>
  );
};

export default ClaimTicketModal;

const ClaimAmount = styled.div`
  span {
    font-weight: 500;
    color: var(--yellow);
  }
`;

const NoTicket = styled.h4`
  padding: 15px 0;
  font-size: 16px;
`;

const CircleNumber = styled.div``;

const SelectWrapper = styled(Select)`
  max-width: 150px;
  width: 100%;
  .ant-select-dropdown {
    background: black;
  }
  .ant-select-selector {
    background-color: #2e3139 !important;
    border: none !important;
    height: 40px !important;
    border-radius: 10px !important;
  }
  .ant-select-selection-item {
    color: var(--yellow);
    line-height: 40px !important;
  }
`;

const ClaimListView = styled.div`
  .reward-item-default {
    width: 45px;
    height: 45px;
    background: rgba(244, 245, 246, 0.2);
    border: 2px solid rgba(73, 228, 84, 0.1);
    border-radius: 1000px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: var(--claim-color-black);
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    position: relative;
    &:hover {
      box-shadow: 0 0 11px rgba(244, 245, 246, 0.2);
    }
    span {
      font-weight: 500;
    }
  }
  .reward-item-active {
    background: rgba(255, 209, 102, 0.2);
    border: 2px solid #ffd166;
    color: white;

    &:hover {
      box-shadow: 0 0 11px rgba(255, 209, 102, 0.4);
    }
  }
  .item-claim-record {
    display: flex;
    gap: 20;
    justify-content: space-between;
    align-items: center;
    .span-flex {
      display: flex;
      gap: 0.34rem;
    }
    &:not(:last-child) {
      margin-bottom: 20px;
    }
  }
  .claim-amount {
    color: var(--yellow);
  }
  .claim-record {
    max-height: 325px;
    height: 100%;
    overflow: auto;
    font-size: 20px;
    padding: 10px 5px;
  }
  .claim-record:last-child {
    padding: 10px 5px 0;
  }
`;

const ClaimWrapModal = styled.div`
  .wrap-modal {
    .wrap-modal.ant-modal .ant-modal-header {
      border-bottom: transparent !important;
    }
  }
`;

const ModalTitleHead = styled.h1`
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  line-height: 24px;
  color: #ffffff;
`;

const ClaimTokenBanner = styled.div`
  background: rgba(255, 209, 102, 0.2);
  /* border: 1px solid #ffd166; */
  border-radius: 12px;
  padding: 15px;
  color: var(--white);
  font-weight: 600;
  .claim-banner-wrapper {
    display: flex;
    align-items: center;
    gap: 15px;
  }

  .claim-banner-winning-total {
    flex: 3;
    text-align: left;
    display: flex;
    align-items: center;
    .claim-banner-winning-total-mid-value {
      padding: 0 10px;
      color: var(--yellow);
    }
    .claim-banner-winning-total-value {
      font-weight: normal;
      background-color: rgba(255, 255, 255, 0.2);
      padding: 8px 15px;
      border-radius: 8px;
    }
    span {
      font-weight: 500;
    }
  }

  .claim-banner-winning-round {
    margin-bottom: 4px;
    span {
      font-weight: 500;
    }
  }
  .claim-banner-winning-round-title {
    text-transform: uppercase;
  }
  .claim-banner-winning-total-title {
    text-transform: capitalize;
    color: var(--yellow);
  }
  @media screen and (max-width: 450px) {
    .claim-banner-logo {
      display: none;
    }
  }
`;

const ClaimTokenBody = styled.div`
  margin-top: 15px;
  .flex {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 12px 0;
  }
  .claim-banner-button {
    flex: 1;
    text-align: right;
    cursor: pointer;
    .claim-btn {
      border-radius: 8px;
      display: inline-block;
      color: white;
      padding: 13px 40px;
      font-weight: 500;
      transition: all 0.3s ease-in-out;
    }
    .claim-btn-active {
      background: linear-gradient(90deg, rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0) 100%), rgb(35, 173, 47);
      &:hover {
        box-shadow: 0 0 11px rgba(35, 173, 47, 0.5);
      }
    }
    .claim-btn-disable {
      background: linear-gradient(90deg, rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0) 100%), #6f6f6f;
    }
  }
`;
