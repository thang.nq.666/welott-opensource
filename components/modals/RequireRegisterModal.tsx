import { Modal } from 'antd';
import { StateContext } from 'context/StateContext';
import { EAction, EModal } from 'define/enum';
import useFunctionContract from 'hooks/useFunctionContract';
import { useCallback, useContext, useMemo } from 'react';

/* eslint-disable @typescript-eslint/no-explicit-any */
type Props = {
  tokenId?: string;
  token?: any;
  textTitle?: string;
  textConfirm?: string;
  textCancel?: string;
  togglePopup?: any;
  handleConfirm?: any;
};

const RequireRegisterModal = ({
  textTitle = 'Register to Welott 🚀 !',
  textConfirm = 'Accept',
  textCancel = 'Cancel',
  handleConfirm,
}: Props) => {
  const { nameOpenModal, dispatch } = useContext(StateContext);
  const { handleRegisterToken } = useFunctionContract();
  const visible = useMemo(() => (nameOpenModal === EModal.REQUIRE_REGISTER ? true : false), [nameOpenModal]);

  const _cancel = useCallback(() => {
    dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.NONE });
  }, [dispatch]);

  const _confirm = useCallback(() => {
    if (handleConfirm) return handleConfirm();
    handleRegisterToken();
    _cancel();
  }, [_cancel, handleConfirm, handleRegisterToken]);

  return (
    <Modal
      centered
      open={visible}
      onOk={_cancel}
      onCancel={_cancel}
      footer={null}
      className={`wrap-modal require-modal`}
    >
      <div className="popup notification">
        <h4 className="text-notification">{textTitle}</h4>
        <div className="wrap-button">
          <button onClick={_cancel} className="button-notification button-gray">
            {textCancel}
          </button>
          <button onClick={_confirm} className="button-notification">
            {textConfirm}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default RequireRegisterModal;
