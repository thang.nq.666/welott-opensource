import { Modal } from 'antd';
import { StateContext } from 'context/StateContext';
import { EAction, EModal } from 'define/enum';
import React, { CSSProperties, ReactElement, useContext } from 'react';

interface IProps {
  className?: string;
  visible: boolean;
  title: string | ReactElement;
  children?: ReactElement;
  style?: CSSProperties;
}

const WrapModal = ({ className = '', visible = false, title, children, style }: IProps) => {
  const { dispatch } = useContext(StateContext);

  return (
    <Modal
      style={style}
      title={title}
      centered
      open={visible}
      onOk={() => dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.NONE })}
      onCancel={() => dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.NONE })}
      footer={null}
      className={`wrap-modal ${className}`}
    >
      {children}
    </Modal>
  );
};

export default WrapModal;
