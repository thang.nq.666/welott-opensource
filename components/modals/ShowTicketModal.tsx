import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';

import WrapModal from './WrapModal';
import { StateContext } from 'context/StateContext';
import { EModal } from 'define/enum';
import useFunctionContract from 'hooks/useFunctionContract';

interface IProps {
  title?: string;
}

const ShowTicketModal = ({ title = 'Your ticket' }: IProps) => {
  const { nameOpenModal } = useContext(StateContext);
  const { handleGetLastLotteryId, handleGetFullUserTicketNumber } = useFunctionContract();
  const [ticketNumbers, setTicketNumbers] = useState<number[]>([]);
  const [lastId, setLastId] = useState(0);

  const visible = useMemo(() => nameOpenModal === EModal.SHOW_TICKET, [nameOpenModal]);
  const mod_title = `${title} (${ticketNumbers?.length || 0})`;

  const _handleGetUserTicket = useCallback(async () => {
    if (!lastId) return;
    const ticket_number = await handleGetFullUserTicketNumber(lastId);
    if (!ticket_number) return;
    setTicketNumbers(ticket_number);
  }, [handleGetFullUserTicketNumber, lastId]);

  const getLastId = useCallback(async () => {
    const last_id = (await handleGetLastLotteryId()) as unknown as number;
    setLastId(last_id);
  }, [handleGetLastLotteryId]);

  useEffect(() => {
    getLastId();
  }, [getLastId]);

  useEffect(() => {
    _handleGetUserTicket();
  }, [_handleGetUserTicket]);

  return (
    <WrapModal className="show-ticket-modal" visible={visible} title={mod_title}>
      <div className="show-ticket-modal-body">
        <div className="list-number-choose">
          {ticketNumbers?.length ? (
            ticketNumbers.map((number, index) => (
              <div key={index} className="wrap-number-choose">
                <div className="number-del number-count">#{index + 1}</div>
                <div className="number-show">
                  {number
                    .toString()
                    .split('')
                    .slice(1)
                    .map((i, idx) => (
                      <div key={idx} className="number-circle">
                        <p>{i}</p>
                      </div>
                    ))}
                </div>
              </div>
            ))
          ) : (
            <p>Not Available</p>
          )}
        </div>
      </div>
    </WrapModal>
  );
};

export default ShowTicketModal;
