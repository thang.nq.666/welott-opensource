/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { InputNumber } from 'antd';
import { Balls } from 'components/common';
import HashLink from 'components/common/HashLink';
import WrapModal from './WrapModal';
import ArrowLeft from 'public/images/arrow-left.svg';
import ArrowRight from 'public/images/arrow-right.svg';
import ArrowRightStop from 'public/images/arrow-right-stop.svg';
import { StateContext } from 'context/StateContext';
import { EAction, EModal } from 'define/enum';
import useFunctionContract from 'hooks/useFunctionContract';
import { ICurrentLottery, ILotteryInfo, IRoundUserInfo } from 'define/interface';
import { ENV_EXPLORER_URL, ENV_ID_TOKEN_wNEAR, tokenFormat, ENV_API_URL } from 'utils/constant';
import useTokenPrices from 'hooks/useTokenPrices';
import { formatCashToView } from 'utils/common';
import axios from 'axios';
import clock from 'public/images/clock.png';
import Image from 'next/image';
import useInitWithApi from 'hooks/useInitWithApi';

interface IProps {
  title?: string;
}

const initTimeRemain = {
  hh: '00',
  mm: '00',
  ss: '00',
};

const LotteryDetailBalls = ({
  final_number,
  current_lottery_id,
}: {
  final_number: string;
  current_lottery_id: string;
}) => {
  const countDownRef = useRef<any>();
  const { nameOpenModal } = useContext(StateContext);
  const visible = useMemo(() => (nameOpenModal === EModal.DETAIL ? true : false), [nameOpenModal]);

  const { handleGetLastLotteryId, handleGetCurrentLotteryRun, handleGetCurrentTimeOnBlockchain } =
    useFunctionContract();
  const [currentLottery, setCurrentLottery] = useState<ICurrentLottery>();
  const [currentTimeBlockchain, setCurrentTimeBlockchain] = useState(0);

  const [timeRemain, setTimeRemain] = useState(initTimeRemain);

  const getCurrentLottery = useCallback(async () => {
    const last_id = await handleGetLastLotteryId();

    if (Number(current_lottery_id) !== last_id) return;

    const get_current = await handleGetCurrentLotteryRun(last_id);
    setCurrentLottery({
      ...get_current,
      lottery_id: last_id,
    });
  }, [current_lottery_id, handleGetCurrentLotteryRun, handleGetLastLotteryId]);

  const _timeRemaining = useCallback(async () => {
    const end_time = new Date((currentLottery?.end_time as unknown as number) / Math.pow(10, 6)).getTime();

    if (currentTimeBlockchain) {
      const time_remaining = (end_time - currentTimeBlockchain) / 1000;
      if (time_remaining < 0) {
        clearInterval(countDownRef.current);
        return setTimeRemain(initTimeRemain);
      }

      let total = time_remaining;
      const hours = Math.floor(total / 3600);
      total %= 3600;
      const minutes = Math.floor(total / 60);
      const seconds = total % 60;

      // Strings with leading zeroes:
      const _hours = String(hours).padStart(2, '0');
      const _minutes = String(minutes).padStart(2, '0');
      const _seconds = Math.floor(Number(seconds)).toFixed().padStart(2, '0');

      if (time_remaining && _hours && _minutes && _seconds) {
        setTimeRemain({
          hh: _hours,
          mm: _minutes,
          ss: _seconds,
        });
      }
    }
  }, [currentLottery?.end_time, currentTimeBlockchain]);

  const _countDown = useCallback(() => {
    if (!visible) return;
    if (currentTimeBlockchain < 0 || !currentTimeBlockchain) return;
    setCurrentTimeBlockchain((prev) => prev + 1000);
  }, [currentTimeBlockchain, visible]);

  const _initCountDown = useCallback(async () => {
    countDownRef.current = setInterval(_countDown, 1000);
  }, [_countDown]);

  const _initGetTimeBlockchain = useCallback(async () => {
    const current_time_blockchain = await handleGetCurrentTimeOnBlockchain();
    const current_time_to_seconds = new Date(
      (current_time_blockchain as unknown as number) / Math.pow(10, 6)
    ).getTime();
    setCurrentTimeBlockchain(current_time_to_seconds);
  }, [handleGetCurrentTimeOnBlockchain]);

  useEffect(() => {
    getCurrentLottery();
  }, [getCurrentLottery]);

  useEffect(() => {
    _initGetTimeBlockchain();
  }, [_initGetTimeBlockchain]);

  useEffect(() => {
    _timeRemaining();
  }, [_timeRemaining]);

  useEffect(() => {
    if (currentLottery?.lottery_id && visible) {
      _initCountDown();
    }
    return () => {
      clearInterval(countDownRef.current);
    };
  }, [_initCountDown, currentLottery?.lottery_id, visible]);

  useEffect(() => {
    if (!visible)
      return () => {
        clearInterval(countDownRef.current);
      };
  }, [visible]);

  return final_number ? (
    <React.Fragment>
      {final_number
        .slice(1)
        .split('')
        .map((item, idx) => (
          <div className="single-lott-number" key={idx}>
            <Balls ballNumber={Number(item)} width={64} height={64} />
          </div>
        ))}
    </React.Fragment>
  ) : (
    <div className="current_info_value current_info_value_lottery_detail">
      <div className="clock-animated-transform-scale">
        <Image priority={true} src={clock} width={79} height={61} alt="Time remaining of current lottery" />
      </div>

      <p className="fix-width">
        <span className="space">
          {timeRemain?.hh || '00'}
          <span className="time">h</span>
        </span>
        :
        <span className="space">
          {timeRemain?.mm || '00'}
          <span className="time">m</span>
        </span>
        :
        <span className="space">
          {timeRemain?.ss || '00'}
          <span className="time">s</span>
        </span>
      </p>
    </div>
  );
};

const LotteryDetailModal = ({ title = 'Detail' }: IProps) => {
  const { dispatch, nameOpenModal, currentLotteryId } = useContext(StateContext);
  const visible = useMemo(() => (nameOpenModal === EModal.DETAIL ? true : false), [nameOpenModal]);
  const typingRef = useRef<any>(null);

  const { tokenPrice } = useTokenPrices({ isVisible: visible });
  const { handleGetNumberTicketsPerLottery, handleGetLastLotteryId, handleGetCurrentLotteryRun } =
    useFunctionContract();
  const { getDetailLottery } = useInitWithApi();

  const [currentLottery, setCurrentLottery] = useState<IRoundUserInfo>();
  const [timeLotteryClose, setTimeLotteryClose] = useState<Date>();
  const [amountTicket, setAmountTicket] = useState(0);
  const [inputId, setInputId] = useState(currentLotteryId);
  const [lastId, setLastId] = useState(1);
  const [hashString, setHashString] = useState<string | undefined>(undefined);

  const _initTimeLotteryClose = useCallback(async () => {
    if (!currentLottery?.id) return;
    const rs = await handleGetCurrentLotteryRun(Number(currentLottery?.id));
    if (!rs) return;

    const end_time = new Date((rs.end_time as unknown as number) / Math.pow(10, 6)).getTime();
    const date = new Date(end_time);

    setTimeLotteryClose(date);
  }, [currentLottery?.id, handleGetCurrentLotteryRun]);

  const _current = useCallback(
    async (_id: number) => {
      setInputId(_id);
      const last_id = await handleGetLastLotteryId();
      const current_lottery = await getDetailLottery(_id);

      if (_id === last_id) {
        const await_open_round = (await handleGetCurrentLotteryRun(last_id)) as unknown as ILotteryInfo;
        const open_round = await_open_round;
        current_lottery.amount_collected_in_near = open_round.amount_collected_in_near;
        current_lottery.rewards_breakdown = open_round.rewards_breakdown.join(',');
      }

      setLastId(last_id);
      setCurrentLottery(current_lottery);
    },
    [getDetailLottery, handleGetCurrentLotteryRun, handleGetLastLotteryId]
  );

  const _ticketPurchase = useCallback(async () => {
    const amount_tickets = await handleGetNumberTicketsPerLottery(inputId);
    setAmountTicket(amount_tickets);
  }, [handleGetNumberTicketsPerLottery, inputId]);

  const current_prices = useMemo(() => {
    const NEAR_DECIMALS =
      tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals;
    return (currentLottery?.amount_collected_in_near as unknown as number) / Math.pow(10, NEAR_DECIMALS);
  }, [currentLottery?.amount_collected_in_near]);

  const current_prices_dollar = useCallback(
    (_price: number) => {
      if (!_price || !tokenPrice) return 0;
      return _price * (tokenPrice?.[ENV_ID_TOKEN_wNEAR] ? tokenPrice?.[ENV_ID_TOKEN_wNEAR] : 1);
    },
    [tokenPrice]
  );
  const _percent_of = useCallback((number: number, percent: number) => number * (percent / 10000), []);
  const cal_treasury_fee = current_prices - current_prices * (Number(currentLottery?.treasury_fee) / 10000);

  const _next = () => {
    if (inputId >= lastId) return;
    dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: inputId + 1 });
  };

  const _prev = () => {
    if (inputId === 1) return;
    dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: inputId - 1 });
  };

  const _latest = () => {
    dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: lastId });
  };

  const _onChange = (value: number | null): void => {
    // debounce
    if (typingRef.current) {
      clearTimeout(typingRef.current);
    }
    typingRef.current = setTimeout(() => {
      if (!value) return;
      if (Number(value) < lastId) dispatch({ type: EAction.SET_CURRENT_LOTTERY_ID, payload: value });
      else {
        _latest();
      }
    }, 500);
  };

  const _getHashLink = useCallback(async () => {
    try {
      if (lastId === inputId) return setHashString(undefined);
      if (!inputId || !visible) return;
      const hashData = await axios.get(`${ENV_API_URL}/hash?lottery_id=${inputId}`);
      if (!hashData.data.data.txHash) return setHashString(undefined);
      setHashString(hashData.data.data.txHash);
    } catch (e) {
      setHashString(undefined);
      console.log(e);
    }
  }, [inputId, lastId, visible]);

  // const _initCurrentLottery = useCallback(async () => {}, [getDetailLottery, inputId]);

  useEffect(() => {
    _current(currentLotteryId);
  }, [_current, currentLotteryId]);

  useEffect(() => {
    _ticketPurchase();
  }, [_ticketPurchase]);

  useEffect(() => {
    _getHashLink();
  }, [_getHashLink]);

  useEffect(() => {
    _initTimeLotteryClose();
  }, [_initTimeLotteryClose]);

  // useEffect(() => {
  //   _initCurrentLottery();
  // }, [_initCurrentLottery]);

  return (
    <WrapModal visible={visible} title={title}>
      <div id="detail-modal">
        <div className="flex">
          <div className="head-info">
            <div className="wrap-round-head">
              <h3>Round</h3>
              <InputNumber
                type={'number'}
                className={'input-custom'}
                min={1}
                max={100000}
                defaultValue={currentLotteryId}
                value={inputId}
                onChange={_onChange}
              />
            </div>
            {hashString && hashString.length > 1 ? (
              <HashLink url={`${ENV_EXPLORER_URL}/transactions/${hashString}`} />
            ) : null}
          </div>
          <div className="head-ticket">
            <p className="cl-smoke-gray">Tickets Purchased: {amountTicket || 0}</p>
          </div>
        </div>
        <div className="wrap-time">
          <p className={`cl-smoke-gray ${currentLottery?.status !== 'Opening' ? '' : 'fake-height'}`}>
            {currentLottery?.status !== 'Opening'
              ? `Drawn ${timeLotteryClose?.toDateString()} 
              ${timeLotteryClose?.toLocaleTimeString()}`
              : ''}
          </p>
        </div>
        <div className="wrap-lott-number">
          <div className="lott-number">
            <LotteryDetailBalls
              current_lottery_id={currentLottery?.id as string}
              final_number={currentLottery?.final_number as string}
            />
          </div>
          <div className="lott-controls">
            <div className="side">
              <div onClick={_prev} className="btn">
                <ArrowLeft />
              </div>
              <div onClick={_next} className="btn mid show-line">
                <ArrowRight />
              </div>
            </div>
            <div className="side right">
              <div onClick={_latest} className="btn">
                <ArrowRightStop />
              </div>
            </div>
          </div>
        </div>
        {currentLottery?.status ? (
          <React.Fragment>
            <p className="desc cl-smoke-gray">
              Match the winning number in the same order to share prizes. Current prizes up for grabs:
            </p>
            <div className="wrap-price">
              <h4>Prize Pot: {formatCashToView(current_prices, 8)} $NEAR</h4>
              <p className="cl-smoke-gray on-desktop">
                (~${formatCashToView(current_prices_dollar(current_prices), 8)})
              </p>
            </div>
            <div className="wrap-board">
              {currentLottery?.rewards_breakdown
                ? currentLottery.rewards_breakdown.split(',').map((reward_percent, idx) => (
                  <div key={idx} className="reward">
                    {idx + 1 < 6 ? (
                      <p className={`title ${idx + 1 && 'cl-green-lott'}`}>Match last {idx + 1}</p>
                    ) : (
                      <p className={`title ${idx + 1 && 'cl-green-lott'}`}>Match All</p>
                    )}
                    <h3 className="amount">
                      {formatCashToView(_percent_of(cal_treasury_fee, Number(reward_percent)), 8)}
                    </h3>
                    {/* <p className="last cl-smoke-gray">
                    $
                    {formatCashToView(
                      current_prices_dollar(_percent_of(cal_treasury_fee, Number(reward_percent))),
                      8
                    )}
                  </p> */}
                    <p className="last cl-smoke-gray">
                      {currentLottery?.counter_winners?.split(',')[idx] || 0} Winning Tickets
                    </p>
                  </div>
                ))
                : 'Not Available'}

              {currentLottery?.rewards_breakdown ? (
                <div className="reward">
                  <p className="title">Reserve pool</p>
                  <h3 className="amount">
                    {formatCashToView(
                      _percent_of(Number(current_prices), currentLottery?.treasury_fee as unknown as number),
                      8
                    )}
                  </h3>
                  <p className="last cl-smoke-gray">
                    $
                    {formatCashToView(
                      current_prices_dollar(
                        _percent_of(Number(current_prices), currentLottery?.treasury_fee as unknown as number)
                      ),
                      8
                    )}
                  </p>
                </div>
              ) : null}
            </div>
          </React.Fragment>
        ) : null}
      </div>
    </WrapModal>
  );
};

export default LotteryDetailModal;
