/* eslint-disable @typescript-eslint/no-explicit-any */
import { Contract, Near, WalletConnection } from 'near-api-js';
import { EAction, EModal } from 'define/enum';
import type { AccountView } from 'near-api-js/lib/providers/provider';

export interface IUserTicket {
  lottery_ticket_ids: number[];
  ticket_numbers: number[];
  ticket_status: number[];
  cursor: number;
  lottery_id?: number;
  winning_number?: number;
}

export interface ILotteryInfo {
  lottery_id: number;
  status: string;
  start_time: number;
  end_time: number;
  price_ticket_in_near: number;
  discount_divisor: number;
  rewards_breakdown: number[];
  treasury_fee: number;
  near_per_bracket: number[];
  count_winners_per_bracket: number[];
  first_ticket_id: number;
  first_ticket_id_next_lottery: number;
  amount_collected_in_near: number;
  final_number: number;
}
export interface ICurrentLottery {
  status: string;
  start_time: number;
  end_time: number;
  price_ticket_in_near: number;
  discount_divisor: number;
  rewards_breakdown: number[];
  treasury_fee: number;
  near_per_bracket: number[];
  count_winners_per_bracket: number[];
  first_ticket_id: number;
  first_ticket_id_next_lottery: number;
  amount_collected_in_near: number;
  final_number: number;
  lottery_id?: number;
  amount_tickets?: number;
}

export type TTokenFormat = {
  [token_id: string]: {
    extra_decimals: number;
    contract_decimals: number;
    icon?: string;
    name: string;
    nameUsd: string;
    usd: number | undefined;
    symbol: string;
    token_id: string;
  };
};

export interface IAction {
  type: EAction;
  payload?: unknown;
}

export interface ConfigProps {
  networkId: string;
  keyStore?: { [key: string]: string | number };
  nodeUrl: string;
  walletUrl: string;
  helperUrl: string;
  explorerUrl: string;
  headers?: { [key: string]: string | number };
}
export interface IFunctionInitState {
  handleNearLogin: () => void;
  handleNearLogOut: () => void;
  handleGetAccountId: () => string;
}

export interface IUserNearBalance {
  available: string;
  staked: string;
  stateStaked: string;
  total: string;
}
export interface IUserStatistic {
  lotteries_won: string[];
  lotteries_joined: string[];
  near_amount_can_claim: number | undefined;
  n_tickets_won: number | undefined;
  n_total_tickets?: number | undefined;
}
export interface IState {
  nameOpenModal: EModal;
  isLoading: boolean;
  isLogin: boolean;
  contract?: IContract;
  contract_oracle?: IContract;
  wallet?: WalletConnection | any;
  near?: Near;
  userNearBalance: IUserNearBalance;
  userStatistic: IUserStatistic;
  currentLotteryId: number;
  roundUserInfo: IRoundUserInfo[];
  pagingHistory: IPagingHistory;
  [key: string]: unknown;
}
export interface IBallColor {
  [key: number]: string;
}

export interface INumber {
  number: number;
}

export interface IStateContext {
  dispatch: React.Dispatch<IAction>;
}

export type WalletBalance = {
  available: string;
  staked: string;
  stateStaked: string;
  total: string;
};

export interface IContractMain {
  get_price_data?: any;
  oracle_call?: any;
  storage_deposit?: any;
}

export interface IArgs {
  cursor?: number;
  size?: number;
}

export interface IUserTicketInRound {
  final_number: number;
  lottery_id: number;
  ticket_ids: number[];
  ticket_numbers: number[];
  ticket_status: number[];
  tickets_rewards: number[];
  cursor: number;
}

export interface IContract extends Contract, IContractMain {}

export interface IContext extends IState, IStateContext {}

export interface IRoundUserInfo {
  id: string;
  counter_winners: string;
  amount_collected_in_near: number;
  treasury_fee: string;
  status: string;
  tickets_len: number;
  ticket_ids: number[];
  ticket_numbers: number[];
  final_number: string;
  near_in_rewards: {
    ticket_number: string;
    rewards: number;
    highest_bracket: number;
    status: number;
    ticket_id?: number;
  }[];
  rewards_breakdown: string;
  near_per_bracket: string[];
  total_rewards: number;
  number_ticket_won: number;
  near_amount_can_claim: number;
}

export interface Message {
  premium: boolean;
  sender: string;
  text: string;
}

export type Account = AccountView & {
  account_id: string;
};

export interface IPagingHistory {
  total: number;
  total_pages: number;
  item_per_page: number;
}
