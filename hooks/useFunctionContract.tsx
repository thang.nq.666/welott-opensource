/* eslint-disable @typescript-eslint/no-explicit-any */
import { StateContext } from 'context/StateContext';
import { useWalletSelector } from 'context/WalletContext';
import { EAction, EModal } from 'define/enum';
import { IUserTicketInRound } from 'define/interface';
import { providers } from 'near-api-js';
import { CodeResult } from 'near-api-js/lib/providers/provider';
import { useCallback, useContext, useEffect } from 'react';
import { injectHtmlToWallet } from 'utils/common';
import { ENV_ID_MAIN_CONTRACT, GAS, ONE_OCTO_STRING, STORAGE_DEPOSIT_FEE } from 'utils/constant';

interface ICommonParam {
  _lottery_id: number;
  _cursor?: number;
  _size?: number;
}

const useFunctionContract = () => {
  const { wallet, isLogin, dispatch } = useContext(StateContext);
  const { selector, modal, accountId } = useWalletSelector();

  const _viewMethod = useCallback(
    (method_name: string, args = {}) => {
      if (!selector) return;
      const { network } = selector.options;
      const provider = new providers.JsonRpcProvider({ url: network.nodeUrl });

      return provider
        .query<CodeResult>({
          request_type: 'call_function',
          account_id: ENV_ID_MAIN_CONTRACT as string,
          method_name: method_name,
          args_base64: Buffer.from(JSON.stringify(args)).toString('base64'),
          finality: 'optimistic',
        })
        .then((res) => JSON.parse(Buffer.from(res.result).toString()))
        .catch((e) => console.log('eeeee > > ', e));
    },
    [selector]
  );

  const _callMethod = useCallback(
    async (method_name: string, args = {}, deposit: string = ONE_OCTO_STRING) => {
      const wallet = await selector.wallet();
      dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.NONE });
      setTimeout(() => {
        dispatch({ type: EAction.ON_LOADING });
      }, 500);
      try {
        const outcome = (await wallet.signAndSendTransaction({
          signerId: accountId as string,
          receiverId: ENV_ID_MAIN_CONTRACT as string,
          actions: [
            {
              type: 'FunctionCall',
              params: {
                methodName: method_name,
                args,
                gas: GAS.toString(),
                deposit,
              },
            },
          ],
        })) as unknown as providers.FinalExecutionOutcome;

        const _isSuccess =
          outcome.transaction_outcome.outcome.status.hasOwnProperty('SuccessReceiptId') ||
          outcome.transaction_outcome.outcome.status.hasOwnProperty('SuccessValue');

        if (_isSuccess) {
          dispatch({ type: EAction.OFF_LOADING });
          window.location.replace(window.location.origin + window.location.pathname);
        } else {
          dispatch({ type: EAction.OFF_LOADING });
        }

        return providers.getTransactionLastResult(outcome);
      } catch (e) {
        dispatch({ type: EAction.OFF_LOADING });
      }
    },
    [accountId, dispatch, selector]
  );

  const handleNearLogOut = useCallback(async () => {
    try {
      if (!selector) return;
      const wallet = await selector.wallet();
      await wallet.signOut();

      localStorage.clear();
      window.location.replace(window.location.origin + window.location.pathname);
    } catch (e) {
      console.log(e);
    }
  }, [selector]);

  const handleNearLogin = useCallback(async () => {
    try {
      if (!wallet || isLogin) {
        return;
      }
      modal.show();
      injectHtmlToWallet();
    } catch (e) {
      console.log('near login > > ', e);
    }
  }, [isLogin, modal, wallet]);

  const handleGetAccountId = useCallback(() => {
    if (!wallet) {
      return '';
    }
    return wallet?.getAccountId() || '';
  }, [wallet]);

  const _validIsUserLogin = useCallback(() => {
    if (!wallet || !selector?.isSignedIn()) {
      return dispatch({ type: EAction.LOGOUT });
    }
    return dispatch({ type: EAction.LOGIN });
  }, [dispatch, selector, wallet]);

  //functionCall
  const handleRegisterToken = useCallback(async () => {
    const args = {
      account_id: accountId,
    };
    return await _callMethod('storage_deposit', args, STORAGE_DEPOSIT_FEE);
  }, [_callMethod, accountId]);

  const handleBuyTicket = useCallback(
    async (_lottery_id: number, _ticket_numbers: number[], _amount: string) => {
      try {
        const args = {
          _lottery_id,
          _ticket_numbers,
          _amount,
        };
        return await _callMethod('buy_tickets', args, _amount);
      } catch (e) {
        console.log(e);
      }
    },
    [_callMethod]
  );

  //viewFunction
  const handleCheckIsRegister = useCallback(async () => {
    try {
      const is_register = await _viewMethod('storage_balance_of', { account_id: accountId });
      if (is_register === '0' || is_register === null) {
        return false;
      }
      return true;
    } catch (err) {
      return false;
    }
  }, [_viewMethod, accountId]);

  const handleGetCurrentTimeOnBlockchain = useCallback(async () => {
    return await _viewMethod('get_current_timestamp');
  }, [_viewMethod]);

  const handleGetNumberTicketsPerLottery = useCallback(
    async (_lottery_id: number) => {
      try {
        const result = await _viewMethod('view_number_tickets_per_lottery', {
          _lottery_id,
        });
        return result;
      } catch (e) {
        console.log(e);
        return 0;
      }
    },
    [_viewMethod]
  );

  const handleGetUserInfoByLotteryId = useCallback(
    async ({ _lottery_id, _cursor = 0, _size = 50 }: ICommonParam) => {
      try {
        if (!!_lottery_id) {
          const result = await _viewMethod('view_user_info_for_lottery_id', {
            _user: accountId,
            _lottery_id,
            _cursor,
            _size,
          });

          return result as IUserTicketInRound;
        }
      } catch (e) {
        console.log(e);
        return;
      }
    },
    [_viewMethod, accountId]
  );

  const handleGetFullUserTicketNumber = useCallback(
    async (last_id: number) => {
      if (!last_id) return;
      let result: number[] = [];
      for await (const index of [0, 50, 100]) {
        const rs = await handleGetUserInfoByLotteryId({ _lottery_id: last_id, _cursor: index });
        if (!rs) {
          result = [];
          break;
        }
        result = [...result, ...rs.ticket_numbers];
      }

      return result;
    },
    [handleGetUserInfoByLotteryId]
  );

  const handleGetCurrentLotteryRun = useCallback(
    async (_lottery_id: number) => {
      return await _viewMethod('view_lottery', { _lottery_id });
    },
    [_viewMethod]
  );

  const handleGetLastLotteryId = useCallback(async () => {
    return await _viewMethod('view_latest_lottery_id', {});
  }, [_viewMethod]);

  const handleGetTotalPriceTickets = useCallback(
    async (_lottery_id: number, _number_tickets: number) => {
      try {
        if (!_number_tickets || !_lottery_id) return '0';
        const total_price = await _viewMethod('calculate_total_price_for_bulk_tickets', {
          _lottery_id,
          _number_tickets,
        });
        if (!total_price) return '0';
        const amountToString = Number(total_price).toLocaleString('fullwide', {
          useGrouping: false,
        });
        return amountToString;
      } catch (e) {
        console.log(e);
        return '0';
      }
    },
    [_viewMethod]
  );

  const handleClaimInRound = useCallback(
    async (_lottery_id: number, _ticket_ids: number[], _brackets: number[]) => {
      try {
        const args = {
          _lottery_id,
          _ticket_ids,
          _brackets,
        };
        return _callMethod('claim_tickets', args, '1');
      } catch (err) {
        console.log(err);
      }
    },
    [_callMethod]
  );

  useEffect(() => {
    _validIsUserLogin();
  }, [_validIsUserLogin]);

  return {
    handleNearLogOut,
    handleNearLogin,
    handleRegisterToken,
    handleCheckIsRegister,
    handleBuyTicket,
    handleGetAccountId,
    handleGetTotalPriceTickets,
    handleGetLastLotteryId,
    handleGetCurrentLotteryRun,
    handleGetUserInfoByLotteryId,
    handleGetNumberTicketsPerLottery,
    handleGetCurrentTimeOnBlockchain,
    handleClaimInRound,
    handleGetFullUserTicketNumber,
  };
};

export default useFunctionContract;
