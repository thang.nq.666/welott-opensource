/* eslint-disable @typescript-eslint/no-explicit-any */
import { useCallback, useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { StateContext } from 'context/StateContext';
import { EAction } from 'define/enum';
import useFunctionContract from './useFunctionContract';
import { ILotteryInfo, IRoundUserInfo, IUserStatistic } from 'define/interface';
import { ENV_API_URL } from 'utils/constant';

const useInitWithApi = () => {
  const { dispatch, contract } = useContext(StateContext);
  const { handleGetCurrentLotteryRun, handleGetLastLotteryId } = useFunctionContract();
  const [lastId, setLastId] = useState(0);

  const getDetailLottery = useCallback(
    async (id: number) => {
      try {
        if (!contract?.account.accountId || !lastId) return;
        const result = await axios.get(
          `${ENV_API_URL}/lottery-detail?account_id=${contract?.account.accountId}&lottery_id=${id}`
        );

        if (!result.data.data.lottery) return;
        return result.data.data.lottery;
      } catch (error) {
        console.error(error);
      }
    },
    [contract?.account.accountId, lastId]
  );

  const _initRoundUserInfo = useCallback(
    async ({ skip = 0, limit = 10 }: { skip?: number; limit?: number }) => {
      try {
        if (!contract?.account.accountId || !lastId) return;
        const result = await axios.get(
          `${ENV_API_URL}/lottery?account_id=${contract?.account.accountId}&skip=${skip}&limit=${limit}`
        );
        const await_open_round = (await handleGetCurrentLotteryRun(lastId)) as unknown as ILotteryInfo;
        const open_round = await_open_round;

        const map_price_for_latest_round =
          skip === 0
            ? result.data.data.lotteries.map((item: IRoundUserInfo) => {
                if (Number(item.id) === lastId) {
                  item.amount_collected_in_near = open_round.amount_collected_in_near;
                  item.rewards_breakdown = open_round.rewards_breakdown.join(',');
                }
                return item;
              })
            : result.data.data.lotteries;

        const final_result = map_price_for_latest_round.map((item: IRoundUserInfo) => ({
          ...item,
          near_in_rewards: item?.near_in_rewards.map((tick: any, index: number) => ({
            ...tick,
            ticket_id: item?.ticket_ids[index],
          })),
        }));

        dispatch({
          type: EAction.INIT_PAGING_HISTORY,
          payload: {
            total: result.data.data.total,
            total_pages: result.data.data.total_pages,
            item_per_page: limit,
          },
        });

        dispatch({
          type: EAction.SET_LOTTERY_INFO_USER,
          payload: final_result,
        });
      } catch (e) {
        console.log(e);
      }
    },
    [contract?.account.accountId, dispatch, handleGetCurrentLotteryRun, lastId]
  );

  const _initUserStatistic = useCallback(async () => {
    try {
      if (!contract?.account.accountId) return;
      const result = await axios.get(`${ENV_API_URL}/statistic?account_id=${contract?.account.accountId}`);

      if (!result.data.status || !result.data.data) return;

      const mod_result: IUserStatistic = {
        ...result.data.data,
        lotteries_joined: result.data.data.lotteries_joined.sort((a: string, b: string) => +a - +b).reverse(),
        lotteries_won: result.data.data.lotteries_won.sort((a: string, b: string) => +a - +b),
      };

      dispatch({
        type: EAction.INIT_USER_STATISTIC,
        payload: mod_result,
      });
    } catch (e) {
      console.log(e);
    }
  }, [contract?.account.accountId, dispatch]);

  const _initLastId = useCallback(async () => {
    const await_last_id = await handleGetLastLotteryId();
    setLastId(await_last_id);
  }, [handleGetLastLotteryId]);

  useEffect(() => {
    _initLastId();
  }, [_initLastId]);

  useEffect(() => {
    _initUserStatistic();
  }, [_initUserStatistic]);

  useEffect(() => {
    _initRoundUserInfo({});
  }, [_initRoundUserInfo]);

  return {
    _initRoundUserInfo,
    getDetailLottery,
  };
};

export default useInitWithApi;
