/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
import { useCallback, useEffect, useRef, useContext } from 'react';
import { TTokenFormat } from 'define/interface';
import { ENV_ID_TOKEN_wNEAR, tokenFormat, TOKEN_SUPPORT_BY_NEARLEND } from 'utils/constant';
import { StateContext } from 'context/StateContext';

const useInitPrice = () => {
  const { contract_oracle } = useContext(StateContext);
  const intervalRef = useRef<any>();

  const _getPriceOracle = useCallback(async () => {
    if (!contract_oracle) return;
    const getUsdtOfToken = async function () {
      try {
        const asset_ids = TOKEN_SUPPORT_BY_NEARLEND;
        let prices = await contract_oracle?.get_price_data({
          asset_ids: asset_ids,
        });
        prices = prices.prices;
        const results: {
          [key: string]: { usd: number | undefined };
        } = {};
        for (let i = 0; i < prices.length; i++) {
          const element = prices[i];
          const price = element.price ? element.price.multiplier / Math.pow(10, 4) : undefined;
          results[ENV_ID_TOKEN_wNEAR] = { usd: price };
        }
        return results;
      } catch (e) {
        console.log(e);
      }
    };
    const price = await getUsdtOfToken();
    if (!price) return;
    const addPriceToTokenFormat = tokenFormat as TTokenFormat;
    Object.entries(price)?.forEach((curr) => {
      const curr_token_id = curr[0];
      for (const key in addPriceToTokenFormat) {
        const { token_id } = addPriceToTokenFormat[key];
        if (curr_token_id === token_id) {
          addPriceToTokenFormat[key].usd = price[curr_token_id].usd;
          tokenFormat[key].usd = price[curr_token_id].usd;
        }
      }
      return addPriceToTokenFormat;
    }, {});
  }, [contract_oracle]);

  const _initPriceUSD = useCallback(async () => {
    intervalRef.current = setInterval(_getPriceOracle, 3000);
  }, [_getPriceOracle]);

  useEffect(() => {
    _initPriceUSD();
    return () => {
      clearInterval(intervalRef.current);
    };
  }, [_getPriceOracle, _initPriceUSD]);

  useEffect(() => {
    _getPriceOracle();
  }, [_getPriceOracle]);

  return null;
};

export default useInitPrice;
