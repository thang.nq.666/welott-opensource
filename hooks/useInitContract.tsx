/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useCallback, useContext } from 'react';
import { EAction } from 'define/enum';
import { ConfigProps } from 'define/interface';
import * as nearAPI from 'near-api-js';
import {
  ENV_EXPLORER_URL,
  ENV_HELPER_URL,
  ENV_ID_MAIN_CONTRACT,
  ENV_ID_NETWORK,
  ENV_ID_ORACLE_CONTRACT,
  ENV_ID_TOKEN_wNEAR,
  ENV_NODE_URL,
  ENV_WALLET_URL,
  tokenFormat,
} from 'utils/constant';
import { formatNearAmount } from 'utils/common';
import { StateContext } from 'context/StateContext';
import { useWalletSelector } from 'context/WalletContext';

const { connect, WalletConnection, keyStores, Contract } = nearAPI;
const nearConfig: ConfigProps = {
  networkId: ENV_ID_NETWORK as unknown as string,
  nodeUrl: ENV_NODE_URL as unknown as string,
  walletUrl: ENV_WALLET_URL as unknown as string,
  helperUrl: ENV_HELPER_URL as unknown as string,
  explorerUrl: ENV_EXPLORER_URL as unknown as string,
  headers: '' || {},
};

const { INIT_CONTRACT, INIT_CONTRACT_ORACLE, INIT_WALLET, INIT_NEAR, INIT_USER_BALANCE } = EAction;

const useInitContract = () => {
  const { dispatch, isLogin } = useContext(StateContext);

  const { accountId } = useWalletSelector();

  const _initConnection = useCallback(async () => {
    try {
      if (typeof window !== 'undefined') {
        const keyStore = new keyStores.BrowserLocalStorageKeyStore();

        const _near = await connect({
          ...nearConfig,
          keyStore: keyStore,
        });

        if (_near) {
          const wallet = new WalletConnection(_near, 'welott-main');
          const _account = await _near.account(accountId as string);

          dispatch({ type: INIT_WALLET, payload: wallet });
          dispatch({ type: INIT_NEAR, payload: _near });

          const contract_main = new Contract(_account, ENV_ID_MAIN_CONTRACT as unknown as string, {
            viewMethods: [],
            changeMethods: ['storage_deposit'],
          });

          dispatch({ type: INIT_CONTRACT, payload: contract_main });
          const myBalance = await _account?.getAccountBalance();

          if (!myBalance || !contract_main?.account?.accountId) return;

          const _available =
            Number(myBalance?.available) /
            Math.pow(
              10,
              tokenFormat[ENV_ID_TOKEN_wNEAR].extra_decimals + tokenFormat[ENV_ID_TOKEN_wNEAR].contract_decimals
            );

          const userNearBalance = {
            ...myBalance,
            available: _available,
            total: formatNearAmount(myBalance?.total),
            stateStaked: formatNearAmount(myBalance?.stateStaked),
          };
          if (!userNearBalance) return;
          dispatch({ type: INIT_USER_BALANCE, payload: userNearBalance });
        }
      }
    } catch (e) {
      console.log('e > > ', e);
    }
  }, [accountId, dispatch]);

  const _initOracle = useCallback(async () => {
    try {
      if (typeof window !== 'undefined') {
        const near = await connect({
          ...nearConfig,
          keyStore: new keyStores.BrowserLocalStorageKeyStore(),
        });
        if (near) {
          const wallet = new WalletConnection(near, 'welott-oracle');
          if (wallet) {
            const contract = new Contract(wallet?.account(), ENV_ID_ORACLE_CONTRACT as unknown as string, {
              viewMethods: ['get_price_data'],
              changeMethods: ['oracle_call'],
            }) as unknown as any;
            dispatch({ type: INIT_CONTRACT_ORACLE, payload: contract });
          }
        }
      }
    } catch (error) {
      console.log('init oracle failed ', error);
    }
  }, [dispatch]);

  useEffect(() => {
    _initOracle();
  }, [_initOracle]);

  useEffect(() => {
    _initConnection();
  }, [_initConnection, isLogin]);

  return null;
};

export default useInitContract;
