import { StateContext } from 'context/StateContext';
import { EAction, EModal } from 'define/enum';
import { useCallback, useContext } from 'react';
import { errorPipeline } from 'utils/common';
import useFunctionContract from './useFunctionContract';

function useActionValidate() {
  const { dispatch, isLogin } = useContext(StateContext);
  const { handleCheckIsRegister } = useFunctionContract();

  const _validBeforeOpenModal = useCallback(
    async (nameModal: string) => {
      const isUserRegister = await handleCheckIsRegister();

      const isLogged = () => {
        console.log('1');
        if (!isLogin && nameModal !== EModal.NONE) {
          dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.REQUIRE_LOGIN });
          return 'not Login yet !';
        }
        return '';
      };

      const isRegister = () => {
        console.log('2');
        if (!isUserRegister && nameModal !== EModal.NONE) {
          dispatch({ type: EAction.TOGGLE_MODAL, payload: EModal.REQUIRE_REGISTER });
          return 'not Register yet !';
        }
        return '';
      };

      const openModal = () => {
        console.log('last');
        if (isLogin && isUserRegister) {
          dispatch({ type: EAction.TOGGLE_MODAL, payload: nameModal });
          return '';
        }
      };

      errorPipeline(isLogged, isRegister, openModal)('ok');
    },
    [dispatch, handleCheckIsRegister, isLogin]
  );

  const handleOpenModal = async (nameModal: string) => {
    return await _validBeforeOpenModal(nameModal);
  };

  return {
    handleOpenModal,
  };
}

export default useActionValidate;
