import { useEffect, useRef } from 'react';

function useEffectOnce(cb: () => unknown) {
  const didRun = useRef(false);

  useEffect(() => {
    if (!didRun.current) {
      cb();
      didRun.current = true;
    }
  });
}

export default useEffectOnce;
