/* eslint-disable @typescript-eslint/no-explicit-any */
import { useRef, useState } from 'react';
import { useCallback, useEffect } from 'react';
import { tokenFormat, TOKEN_SUPPORT_BY_NEARLEND } from 'utils/constant';

interface TokenPrice {
  [key: string]: number;
}
interface IProps {
  isVisible: boolean;
}

const useTokenPrices = ({ isVisible = true }: IProps) => {
  const formatTokenRef = useRef<any>();

  const [tokenPrice, setTokenPrice] = useState<TokenPrice>();

  const _setUpTokenFormat = useCallback(() => {
    if (!tokenFormat) {
      return;
    }
    TOKEN_SUPPORT_BY_NEARLEND.map((token_id) =>
      setTokenPrice((prev) => ({ ...prev, [token_id]: tokenFormat[token_id].usd || 1 }))
    );
  }, []);

  const _init = useCallback(() => {
    formatTokenRef.current = setInterval(_setUpTokenFormat, 3000);
  }, [_setUpTokenFormat]);

  useEffect(() => {
    if (!isVisible) return;
    _init();
    return () => {
      clearInterval(formatTokenRef.current);
    };
  }, [_init, isVisible]);

  return { tokenPrice };
};

export default useTokenPrices;
