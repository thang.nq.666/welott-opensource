export const welottText = {
  max_120: 'The maximum number of tickets that can be purchased in a round is 120',
  max_12: 'The maximum number of tickets that can be purchased in a single transaction is 12.',
  price_per_ticket: 'Ticket prices may be adjusted according to the actual conditions of the protocol.',
  wallet_insert_warning:
    'By connecting to wallet & moving forward, you confirm that you fully understand the risk of playing Welott.',
};
