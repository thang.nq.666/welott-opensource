/* eslint-disable @typescript-eslint/no-explicit-any */
import { ball_colors } from './colors';
import * as nearAPI from 'near-api-js';
import { FORMAT_CASH_DECIMAL } from './constant';
import { welottText } from './text';
const { utils } = nearAPI;

const numList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

export const colorsMapping = (number: number) => {
  return numList.includes(number) ? ball_colors[number] : ball_colors[0];
};

export const formatNearAmount = (amount: string) => {
  if (amount !== 'null') {
    return utils?.format?.formatNearAmount(amount);
  }
  return '0';
};

export const formatAccountName = (accountName: string) => {
  if (accountName?.length < 25) return accountName;
  const first = accountName.slice(0, 3).toString();
  const last = accountName.slice(-3).toString();
  const combine = `${first}...${last}`;
  return combine;
};

export const errorPipeline =
  (...validatedFns: any[]) =>
  (value: unknown) => {
    return validatedFns.reduce((acc, fn) => {
      if (acc) return acc;
      return fn(value);
    }, '');
  };

export const formatCash = (n: number, decimals = FORMAT_CASH_DECIMAL) => {
  if (!n) return '0.00';

  if (n < 1e3 || n < 1e6) {
    return n?.toFixed(decimals).slice(0, -6);
  }

  if (n < 1e3) {
    return new Intl.NumberFormat('en-IN', {
      maximumSignificantDigits: 6,
    })
      .format(n)
      .slice(0, 3);
  }
  if (n <= 1e6) return n?.toFixed(decimals).slice(0, -3);
  if (n >= 1e6 && n < 1e9) {
    return +n.toFixed(decimals).toString().slice(0, -6);
  }
  if (n >= 1e9 && n < 1e12) {
    return +(n / 1e9).toFixed(decimals).slice(0, -6) + 'B';
  }
  if (n >= 1e12) {
    return +(n / 1e12).toFixed(decimals).toString().slice(0, -6) + 'T';
  }
};

export const formatCashToView = (value: number, decimal = 11) => {
  if (!value) return '0.00';
  const numberCheck = Number(formatCash(value, 13));

  const check_min_number = numberCheck < 0.0000001;

  if (check_min_number) return '0.00';

  //format for Thousand number with comma
  if (numberCheck <= 1e6 && numberCheck > 1e3) {
    const result = formatCash(value, decimal) as unknown as string;
    const dotIndex = result.toString().indexOf('.') - 3;

    const split_result_1 = result.toString().slice(0, dotIndex).toString() + ',';
    const split_result_2 = result.toString().slice(dotIndex);
    const final_result = split_result_1.concat(split_result_2);

    return final_result;
  }
  //format for Million number with comma
  if (numberCheck > 1e3 && numberCheck <= 1e9) {
    const dot_index = value.toString().indexOf('.');
    const get_number = value.toString().slice(0, dot_index);
    return get_number.replace(/(.)(?=(\d{3})+$)/g, '$1,');
  }

  return formatCash(value, decimal) || '0.00';
};

export const matchingNumbers = (_final_number: number, _ticket_numbers: number[]) => {
  if (!_final_number || !_ticket_numbers) return;
  const final_number_re = _final_number.toString().slice(1).split('').reverse();
  const match: any = [];
  _ticket_numbers.forEach((t) => {
    let pivot = true;
    const tmp_m = [];
    const s = t.toString().split('').slice(1).reverse();

    for (let i = 0; i < s.length; i++) {
      if (s[i] !== final_number_re[i]) {
        pivot = false;
      }
      const obj = {
        status: pivot,
        number: s[i],
      };
      tmp_m.push(obj);
    }
    match.push({
      ticket: t,
      final: _final_number,
      match: tmp_m.reverse(),
    });
  });
  return match;
};

// calculate Reward per Ticket
export const getRewardByTicket = (final_number: number, ticket_number: number, near_per_bracket: number[]) => {
  const highest_position = calculateHighestPosition(final_number, ticket_number);
  const rewards = near_per_bracket?.[highest_position] || 0;
  return { ticket_number: ticket_number, rewards: rewards / Math.pow(10, 24), highest_bracket: highest_position };
};

const calculateHighestPosition = (final_number: number, ticket_number: number) => {
  const brackets = [1, 11, 111, 1111, 11111, 111111];
  const _bracket_positions = [5, 4, 3, 2, 1, 0];
  let highest_position = -1;
  for (const bracket_position in _bracket_positions) {
    const bracket_number = brackets[bracket_position];
    const transformed_winning_number = bracket_number + (ticket_number % Math.pow(10, Number(bracket_position) + 1));
    const transformed_user_number = bracket_number + (final_number % Math.pow(10, Number(bracket_position) + 1));
    if (transformed_winning_number === transformed_user_number) {
      if (Number(bracket_position) > highest_position) {
        highest_position = Number(bracket_position);
      }
    }
  }
  return highest_position;
};

export const injectHtmlToWallet = () => {
  setTimeout(() => {
    const getWalletModalId = document.getElementById('near-wallet-selector-modal');
    if (getWalletModalId) {
      const parent = document.getElementsByClassName('wallet-info-wrapper');
      const getWindownSize = window.innerWidth;

      if (parent && getWindownSize > 576) {
        const elInsert = document.createElement('div');
        elInsert.classList.add('wallet-what', 'custom-wallet-what');
        elInsert.innerText = welottText.wallet_insert_warning;
        const walletWhat = parent[0].childNodes;
        for (let i = 0; i < walletWhat.length; i++) {
          if (i === walletWhat.length - 3) {
            const element = walletWhat[i];
            if (element) {
              insertAfter(elInsert, element);
            }
          }
        }
      } else {
        const mobileOnly = document.getElementsByClassName('wallet-home-wrapper');
        if (mobileOnly && mobileOnly[0]) {
          const node = document.createElement('div');
          const textnode = document.createTextNode(welottText.wallet_insert_warning);
          node.appendChild(textnode);

          mobileOnly[0].appendChild(node);
        }
      }
    }
  }, 200);
};
function insertAfter(newNode: any, existingNode: any) {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}
